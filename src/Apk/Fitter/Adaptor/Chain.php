<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Adaptor;

use Apk\Fitter\Consumer\ArrayIterator;
use Apk\Fitter\Iterator;

/**
 * Class Chain
 * @package Apk\Fitter\Adaptor
 *
 * Chains multiple iterators, returning all the items from the first, then all from the second, etc.
 */
class Chain extends Iterator
{
	/** @var  \Iterator[] $iterators */
	protected $iterators;
	
	protected $iterator = 0;
	protected $index = 0;
	
	/**
	 * Zip constructor.
	 *
	 * @param Iterator|array|\Traversable ...$args
	 *      Takes a list of iterators to merge
	 */
	public function __construct(...$args)
	{
		foreach ($args as $arg) {
			if (is_array($arg)) {
				$arg = new ArrayIterator($arg);
				
			} elseif (!$arg instanceof \Iterator) {
				throw new \InvalidArgumentException('Zip arguments must all be Iterators');
			}
			
			$this->iterators[] = $arg;
		}
	}
	
	public function current()
	{
		return $this->iterators[$this->iterator]->current();
	}
	
	public function next()
	{
		$this->iterators[$this->iterator]->next();
		$this->index++;
	}
	
	public function key()
	{
		return $this->index;
	}
	
	public function valid()
	{
		if ($this->iterators[$this->iterator]->valid()) {
			return true;
		}
		
		if ($this->iterator < count($this->iterators)-1) {
			$this->iterator++;
			return $this->valid();
		} else {
			return false;
		}
	}
	
	public function rewind()
	{
		foreach ($this->iterators as $iter) {
			$iter->rewind();
		}
		$this->iterator = 0;
		$this->index = 0;
	}
}
 
