<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Adaptor;

use Apk\Fitter\BaseIterator;
use Apk\Fitter\Consumer\ConsumerInterface;

/**
 * Class ConsumerAdaptor
 * @package Apk\Fitter\Adaptor
 *
 * Allows to register a Consumer as it were an adaptor. Usually used by toConsumer()
 */
class ConsumerAdaptor extends BaseIterator
{
	protected $consumer;
	
	/**
	 * ConsumerAdaptor constructor.
	 *
	 * @param array|\ArrayIterator|\Traversable $fromIterator
	 *      Iterator to attach to
	 *
	 * @param ConsumerInterface|mixed           $consumer
	 *      Consumer that will be called before the iteration, at each step and after the iteration
	 */
	public function __construct($fromIterator, $consumer)
	{
		if (!($consumer instanceof ConsumerInterface) && !method_exists($consumer, 'write')) {
			throw new \InvalidArgumentException('Consumer does not support write method');
		}
		
		parent::__construct($fromIterator);
		$this->consumer = $consumer;
	}
	
	/**
	 * Prepares the consumer calling open() on it (if implemented), then iterates over the iterator
	 * and calls close() (if implemented) at the end.
	 */
	public function run()
	{
		if (method_exists($this->consumer, 'open')) {
			$this->consumer->open();
		}
		
		foreach ($this as $item) {
			$this->consumer->write($item);
		}
		
		if (method_exists($this->consumer, 'close')) {
			$this->consumer->close();
		}
	}
	
}
