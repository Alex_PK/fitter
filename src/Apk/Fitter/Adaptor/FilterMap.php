<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Adaptor;

use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Option;

/**
 * Class Filter
 * @package Apk\Fitter\Adaptor
 *
 * Filters the elements of the array and map them at the same time,
 * returning only those that return Some($value) in the given function
 */
class FilterMap extends Iterator
{
	protected $filterMapFunc = null;
	
	/**
	 * @param array|\ArrayIterator|\Traversable $iter
	 *      Iterator to attach the filtering function to
	 *
	 * @param \Closure                          $filterMapFunc
	 *      $filterFunc($element) : Option
	 *      This function will be called for each element in the iterator.
	 *      If it returns Some($value), the element will be mapped to $value.
	 *      If it returns None, the element will be ignored.
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($iter, $filterMapFunc)
	{
		if (!is_callable($filterMapFunc)) {
			throw new \InvalidArgumentException('Filter function must be callable');
		}
		
		parent::__construct($iter);
		$this->filterMapFunc = $filterMapFunc;
	}
	
	public function valid()
	{
		while (parent::valid()) {
			$res = call_user_func($this->filterMapFunc, parent::current(), parent::key());
			if (!$res instanceof Option || $res->isNone()) {
				$this->cachedCurrent = null;
				parent::next();
				
			} else {
				$this->cachedCurrent = $res->unwrap();
				break;
			}
		}
		
		return parent::valid();
	}
}
