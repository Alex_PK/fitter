<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Adaptor;

use Apk\Fitter\Iterator;

/**
 * Class Map
 * @package Apk\Fitter\Adaptor
 *
 * Maps every element in the iterator to a tuple (index, element)
 */
class MapKey extends Iterator
{
	protected $mapFunc = null;
	protected $key = null;
	
	/**
	 * @param array|\ArrayIterator|\Traversable $iter
	 *      The iterator to attache the mapping function to.
	 *
	 * @param callable|\Closure                 $mapFunc
	 *      function($element) : [$index, $newElement]
	 *      This function will be called with every element in turn. It must return a 2-element array.
	 *      The first element of the array will be the index to which the element will be mapped to.
	 *      The second is the element to be mapped. Could be the original element, its transformation or anything else.
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($iter, $mapFunc)
	{
		if (!is_callable($mapFunc)) {
			throw new \InvalidArgumentException('Filter function must be callable');
		}
		
		parent::__construct($iter);
		$this->mapFunc = $mapFunc;
	}
	
	public function current()
	{
		$result = call_user_func($this->mapFunc, parent::current(), parent::key());
		if (!is_array($result) || !is_scalar($result[0])) {
			throw new \UnexpectedValueException('map() must return a [$key, $value] array, and $key must be scalar.');
		}
		$this->key = $result[0];
		
		return $result[1];
	}
	
	public function key()
	{
		return $this->key;
	}
}
