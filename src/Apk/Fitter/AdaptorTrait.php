<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter;

use Apk\Fitter\Adaptor\Chain;
use Apk\Fitter\Adaptor\Filter;
use Apk\Fitter\Adaptor\FilterMap;
use Apk\Fitter\Adaptor\MapKey;
use Apk\Fitter\Adaptor\Skip;
use Apk\Fitter\Adaptor\Take;
use Apk\Fitter\Adaptor\Map;
use Apk\Fitter\Adaptor\Zip;

/**
 * Class AdaptorTrait
 * @package Apk\Iterators
 *
 * This trait implements all the library adaptors, to be attached to an iterator class
 */
trait AdaptorTrait
{
	/**
	 * Maps every element in the iterator to a tuple (index, element)
	 *
	 * @param callable $mapKeyFunc
	 *      $mapFunc($element) : [$index, $newElement]
	 *      This function will be called with every element in turn. It must return a 2-element array.
	 *      The first element of the array will be the index to which the element will be mapped to.
	 *      The second is the element to be mapped. Could be the original element, its transformation or anything else.
	 *
	 * @return MapKey      Iterator that will automatically call the callable on every element
	 */
	public function mapKey(callable $mapKeyFunc)
	{
		return new MapKey($this, $mapKeyFunc);
	}
	
	/**
	 * Filters the elements of the array, returning only those that match (return true) the given function
	 *
	 * @param callable $filterFunc
	 *      $filterFunc($element) : bool
	 *      This function will be called for each element in the iterator.
	 *      If it returns true, the element will go through.
	 *      If it returns false, the element will be ignored.
	 *
	 * @return Filter   Iterator thet will only return elements for which the function returns true
	 */
	public function filter(callable $filterFunc = null)
	{
		return new Filter($this, $filterFunc);
	}
	
	/**
	 * Filters the elements of the array and map them at the same time,
	 * returning only those that return Some($value) in the given function
	 *
	 * @param callable $filterMapFunc
	 *      $filterMapFunc($element) : Option
	 *      This function will be called for each element in the iterator.
	 *      If it returns Some($value), the element will be mapped to $value.
	 *      If it returns None, the element will be ignored.
	 *
	 * @return FilterMap
	 */
	public function filterMap(callable $filterMapFunc = null)
	{
		return new FilterMap($this, $filterMapFunc);
	}
	
	/**
	 * Executes the function on every element of the array
	 *
	 * @param callable $mapFunc
	 *      $walkFunc($element) -> $newElement
	 *      The function will be called on every element and can return the element itself, a modified version of it,
	 *      the result of any calculations or anything else that will be considered the new element in the next
	 *      step of the computation.
	 *
	 * @return Map     Iterator calling the function on every element
	 */
	public function map(callable $mapFunc = null)
	{
		return new Map($this, $mapFunc);
	}
	
	/**
	 * Skips a certain number of elements at the beginning of the iterator
	 *
	 * @param int $numSkip
	 *      Number of elements to skip (ignore) when processing the iterator.
	 *
	 * @return Skip     Iterator skipping its first $numSkip elements while processing
	 */
	public function skip(int $numSkip = 0)
	{
		return new Skip($this, $numSkip);
	}
	
	/**
	 * Takes up to a certain number of elements from the iterator, then ends.
	 *
	 * @param int $numTake
	 *      Number of elements to take from the iterator
	 *
	 * @return Take     Iterator that will only pass $numTake elements through
	 */
	public function take(int $numTake = 0)
	{
		return new Take($this, $numTake);
	}
	
	/**
	 * Merges several iterators, returning at each step an array of elements, one from each iterator.
	 * The process will stop when the shortest iterator is exhausted.
	 *
	 * @param Iterator|array|\Traversable ...$args
	 *      Takes a list of iterators to merge
	 *
	 * @return Zip      Iterator that will return an array of other iterators elements at each step
	 */
	public function zip(...$args)
	{
		$iterators = array_merge([$this], $args);
		$newClass = new \ReflectionClass('Apk\Fitter\Adaptor\Zip');
		return $newClass->newInstanceArgs($iterators);
	}
	
	/**
	 * Chains multiple iterators, returning all the items from the first, then all from the second, etc.
	 *
	 * @param Iterator|array|\Traversable ...$args
	 *      Takes a list of iterators to chain
	 *
	 * @return Chain      Iterator that will return elements from all iterators, one after the other
	 */
	public function chain(...$args)
	{
		$iterators = array_merge([$this], $args);
		$newClass = new \ReflectionClass('Apk\Fitter\Adaptor\Chain');
		return $newClass->newInstanceArgs($iterators);
	}
}
