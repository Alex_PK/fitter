<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter;

/**
 * Class BaseIterator
 * @package Apk\Iterators
 *
 * Implements the basic iterator functionality while caching the current element
 *
 */
class BaseIterator implements \Iterator
{
	protected $cachedCurrent = null;
	protected $currentIsCached = false;
	
	/** @var  \Iterator */
	protected $iter;
	
	/**
	 * @param \Traversable|array|\ArrayIterator $iter
	 */
	public function __construct($iter)
	{
		if (!(is_array($iter) || $iter instanceof \Traversable)) {
			throw new \InvalidArgumentException("Must be a Traversable or an array");
		}
		if (is_array($iter)) {
			$this->iter = new \ArrayIterator($iter);
		} else {
			$this->iter = $iter;
		}
	}
	
	/**
	 * @return mixed
	 */
	public function current()
	{
		// Workaround for lazy iterators
		if (false === $this->currentIsCached) {
			$this->cachedCurrent = $this->iter->current();
			$this->currentIsCached = true;
		}
		
		return $this->cachedCurrent;
	}
	
	public function next()
	{
		$this->cachedCurrent = null;
		$this->currentIsCached = false;
		$this->iter->next();
	}
	
	/**
	 * @return mixed
	 */
	public function key()
	{
		return $this->iter->key();
	}
	
	/**
	 * @return bool
	 */
	public function valid()
	{
		return ($this->currentIsCached || $this->iter->valid());
	}
	
	public function rewind()
	{
		$this->cachedCurrent = null;
		$this->currentIsCached = false;
		$this->iter->rewind();
	}
}
