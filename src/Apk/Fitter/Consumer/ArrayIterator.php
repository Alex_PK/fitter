<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Consumer;

use Apk\Fitter\ConsumerTrait;
use Apk\Fitter\AdaptorTrait;

/**
 * Class ArrayIterator
 * @package Apk\Fitter\Consumer
 *
 * An iterator acting like an array, allowing random access to elements
 */
class ArrayIterator extends \ArrayIterator
{
	use ConsumerTrait;
	use AdaptorTrait;
	
	/**
	 * Creates a PHP array out of the values in the iterator
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->getArrayCopy();
	}
}
