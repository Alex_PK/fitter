<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Consumer;

/**
 * Interface ConsumerInterface
 * @package Apk\Fitter\Consumer
 *
 * Allow to implement a consumer suitable for the toConsumer() function.
 */
interface ConsumerInterface
{
	/**
	 * Will be executed before the iteration begins
	 */
	public function open();
	
	/**
	 * Will be executed at each step of the iteration
	 *
	 * @param mixed $data
	 *      The current iteration step data
	 */
	public function write($data);
	
	/**
	 * Will be called at the end of the iteration
	 */
	public function close();
}
