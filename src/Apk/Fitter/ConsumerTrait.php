<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter;

use Apk\Fitter\Adaptor\ConsumerAdaptor;
use Apk\Fitter\Consumer\ArrayIterator;
use Apk\Fitter\Consumer\ConsumerInterface;

trait ConsumerTrait
{
	/**
	 * Counts the number of elements in the iterator by going through all of them
	 *
	 * @return int      Number of elements in the iterator
	 */
	public function count(): int
	{
		$result = 0;
		foreach ($this as $elem) {
			$result++;
		}

		return $result;
	}

	/**
	 * Calculates the minimum among the values in the iterator
	 *
	 * @param callable|null $compareFunc
	 *      $compareFunc(mixed $element, mixed|null $previousMin) : -1|0|1
	 *      This function should return -1 if $element < $previousMin, 0 if they are equal, +1 if greater.
	 *      If the values are already numeric or string, the function is optional and normal < and > will be used.
	 *
	 * @return mixed    The minimum value or element at the end of the calculation
	 *
	 * @throws \InvalidArgumentException
	 */
	public function min(callable $compareFunc = null)
	{
		$result = null;

		if (is_null($compareFunc) || !is_callable($compareFunc)) {
			$compareFunc = function ($el, $prev = null) {
				return $el <=> $prev;
			};
		}

		foreach ($this as $elem) {
			if (is_null($result) || $compareFunc($elem, $result) == -1) {
				$result = $elem;
			}
		}

		return $result;
	}

	/**
	 * Calculates the maximum among the values in the iterator
	 *
	 * @param callable|null $compareFunc
	 *      $compareFunc(mixed $element, mixed|null $previousMax) : -1|0|1
	 *      This function should return -1 if $element < $previousMax, 0 if they are equal, +1 if greater
	 *      If the values are already numeric or string, the function is optional and normal < and > will be used.
	 *
	 * @return mixed    The maximum value or element at the end of the calculation
	 *
	 * @throws \InvalidArgumentException
	 */
	public function max(callable $compareFunc = null)
	{
		$result = null;

		if (is_null($compareFunc) || !is_callable($compareFunc)) {
			$compareFunc = function ($el, $prev = null) {
				if ($el > $prev) {
					return 1;
				}

				return -1;
			};
		}

		foreach ($this as $elem) {
			if (is_null($result) || $compareFunc($elem, $result) == 1) {
				$result = $elem;
			}
		}

		return $result;
	}

	/**
	 * Calculates the average among the iterator elements.
	 *
	 * @param callable|null $valueFunc
	 *      $valueFunc(mixed $element) : float|int
	 *      The function should return a numeric value for the given element of the iterator.
	 *      The average will be calculated on these numerical values.
	 *      If the elements are already numeric, the function is optional
	 *
	 * @return float|int    The calculated average value
	 */
	public function avg(callable $valueFunc = null)
	{
		if (is_null($valueFunc) || !is_callable($valueFunc)) {
			$valueFunc = function ($el) {
				return floatval($el);
			};
		}

		$avg = 0;
		$count = 0;

		foreach ($this as $elem) {
			$temp = $valueFunc($elem);
			if (!is_numeric($temp)) {
				throw new \UnexpectedValueException('Value function return value must be numeric');
			}

			$count++;
			if ($count == 1) {
				$avg = (float)$temp;
			} else {
				$avg += ($temp - $avg) / $count;
			}
		}

		return $avg;
	}

	/**
	 * Returns the first element in the iterator for which the function returns true.
	 *
	 * @param callable $findFunc
	 *      $findFunc(mixed $element, mixed $key) : bool
	 *      Returns true for the element that needs to be found.
	 *
	 * @return mixed    Returns the first element for which the function is true
	 */
	public function find(callable $findFunc)
	{
		foreach ($this as $key => $elem) {
			if ($findFunc($elem, $key)) {
				return $elem;
			}
		}

		return null;
	}

	/**
	 * "Folds" an iterator to a single value
	 *
	 * @param mixed    $base The basic value to start from
	 * @param callable $foldFunc
	 *                       function(mixed $element, mixed $previousFold) : mixed
	 *                       Calculates the new folded value based on the element and the previously folded value.
	 *
	 * @return mixed    The result of the folding
	 */
	public function fold($base, callable $foldFunc)
	{
		$val = $base;
		foreach ($this as $elem) {
			$val = $foldFunc($elem, $val);
		}

		return $val;
	}

	/**
	 * Reduces an iterator by calculating a fold for each index.
	 * Normal PHP arrays do not allow multiple elements with the same index, so most of the times this function will
	 * be chained to a map() result (iterators can have the same index for multiple values).
	 *
	 * @param callable                $reduceFunc
	 *      $reduceFunc(mixed $element, mixed|null $previousResult, mixed $index) : mixed
	 *      Receives the current element and the previous collector for the element index and should
	 *      return the new value associated with the index.
	 *
	 * @param \ArrayAccess|array|null $resultObject
	 *      The resulting array-capable object to collect the results.
	 *      If null, an ArrayIterator will be used
	 *
	 * @return ArrayIterator|array|\ArrayAccess
	 */
	public function reduce(callable $reduceFunc, $resultObject = null)
	{
		if (is_null($resultObject)) {
			$resultObject = new ArrayIterator();
		} else {
			if (!is_array($resultObject) && !$resultObject instanceof \ArrayAccess) {
				throw new \InvalidArgumentException("resultObject must implement ArrayAccess interface");
			}
		}

		foreach ($this as $key => $el) {
			if (!array_key_exists($key, $resultObject)) {
				$resultObject[$key] = null;
			}
			$resultObject[$key] = $reduceFunc($el, $resultObject[$key], $key);
		}

		return $resultObject;
	}

	/**
	 * Groups the elements in the Iterator by assigning them a key via the provided function
	 *
	 * @param callable                $groupFunc
	 *      $groupFunc(mixed $element, mixed $index): mixed
	 *      Receives the current element and its index in the Iterator and should
	 *      return the new index to group the element under in the result
	 *
	 * @param \ArrayAccess|array|null $resultObject
	 *      An object implementing ArrayAccess or an array in which to store the resulting groups
	 *
	 * @param string|null             $containingObjects
	 *      The name of the class to instantiate for each group, to collect all the elements
	 *      belonging to that group. If null is passed, an empty array will be created.
	 *
	 * @return ArrayIterator|array|\ArrayAccess|null
	 */
	public function group(callable $groupFunc, $resultObject = null, string $containingObjects = null)
	{
		if (is_null($resultObject)) {
			$resultObject = [];
		} else {
			if (!is_array($resultObject) && !$resultObject instanceof \ArrayAccess) {
				throw new \InvalidArgumentException("resultObject must implement ArrayAccess interface");
			}
		}

		foreach ($this as $key => $el) {
			$newIndex = $groupFunc($el, $key);
			if (!array_key_exists($newIndex, $resultObject)) {
				if (is_null($containingObjects)) {
					$resultObject[$newIndex] = [];
				} else {
					$resultObject[$newIndex] = new $containingObjects();
				}
			}
			$resultObject[$newIndex][] = $el;
		}

		return $resultObject;
	}

	/**
	 * Processes all the adaptors and consumers and returns no value.
	 * Useful when the consumers already do something with the data (save them to disk, send to net, etc.)
	 *
	 * @return void
	 */
	public function run()
	{
		foreach ($this as $el) {
			// Do nothing
		}
	}

	/**
	 * Collects the results of the iterator processing into a PHP array
	 *
	 * @param bool $keepKeys
	 *      If set to true, the resulting array will keep the keys
	 *
	 * @return array
	 */
	public function toArray(bool $keepKeys = false)
	{
		$result = [];
		foreach ($this as $key => $el) {
			if ($keepKeys) {
				$result[$key] = $el;
			} else {
				$result[] = $el;
			}
		}

		return $result;
	}

	/**
	 * Collects the results of the iterator processing into an object with an array-like interface
	 *
	 * @param null|\ArrayAccess $targetObject
	 *      The object to use as a collector. If null, an ArrayIterator will be created
	 *
	 * @param bool $keepKeys
	 *      If set to true, the resulting array will keep the keys
	 *
	 * @return array|\ArrayAccess   The collector for the results of the iterator processing
	 */
	public function collect($targetObject = null, bool $keepKeys = false)
	{
		if (is_null($targetObject)) {
			$targetObject = new ArrayIterator();

		} else {
			if (!$targetObject instanceof \ArrayAccess) {
				throw new \InvalidArgumentException('Provided target object must implement \ArrayAccess');
			}
		}

		foreach ($this as $key => $el) {
			if ($keepKeys) {
				$targetObject[$key] = $el;
			} else {
				$targetObject[] = $el;
			}
		}

		return $targetObject;
	}

	/**
	 * Appends a consumer object to the processing queue.
	 * This is more flexible than using a "simple" ArrayAccess object or an array, for example to open and close
	 *  a file or a DB connection before and after processing.
	 *
	 * @param ConsumerInterface|object $consumer
	 *
	 * @return ConsumerAdaptor  An iterator that will call the consumer during the data processing
	 */
	public function toConsumer($consumer)
	{
		return new ConsumerAdaptor($this, $consumer);
	}

	/**
	 * Apply all the functions contained in this Iterator to all the data contained
	 * in the provided Iterator, generating a new Iterator with all the results.
	 *
	 * @param Iterator $data
	 *
	 * @return Iterator
	 */
	public function apply(Iterator $data): Iterator
	{
		return Iterator::from(
			array_reduce(
				$this->toArray(),
				function ($acc, callable $fn) use ($data) {
					return array_merge(
						$acc,
						array_map($fn, $data->toArray())
					);
				},
				[]
			)
		);
	}
}
