<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter;

use Apk\Fitter\Types\Err;
use Apk\Fitter\Types\Id;
use Apk\Fitter\Types\None;
use Apk\Fitter\Types\Ok;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Result;
use Apk\Fitter\Types\Some;

/**
 * Class F
 *
 * @package Apk\Fitter
 *
 * Provides helper functions to ease wrapping of values in functional data types
 */
final class F
{
	/**
	 * The identity function. Simply returns any value it is passed
	 *
	 * @param mixed $v
	 *
	 * @return mixed
	 */
	public static function identity($v)
	{
		return $v;
	}
	
	/**
	 * Composes two functions:
	 *
	 * compose($f1)($f2)($value) = $f1($f2($value)
	 *
	 * @param callable $f1
	 *
	 * @return \Closure
	 */
	public static function compose(callable $f1)
	{
		return function (callable $f2) use ($f1) {
			return function ($x) use ($f1, $f2) {
				return $f1($f2($x));
			};
		};
	}

	/**
	 * Wraps the value into an Id monad
	 *
	 * @param mixed $v
	 *
	 * @return Id
	 */
	public static function id($v): Id
	{
		return new Id($v);
	}

	/**
	 * Wraps the value into a Some Option
	 *
	 * @param mixed $v
	 *
	 * @return Option
	 */
	public static function some($v): Option
	{
		return new Some($v);
	}
	
	/**
	 * Creates a None Option
	 *
	 * @return Option
	 */
	public static function none(): Option
	{
		return new None();
	}
	
	/**
	 * Wraps the value into an Ok Result
	 *
	 * @param mixed $v
	 *
	 * @return Result
	 */
	public static function ok($v): Result
	{
		return new Ok($v);
	}
	
	/**
	 * Wraps the exceoption into an Err Result
	 *
	 * @param \Throwable $e
	 *
	 * @return Result
	 */
	public static function err(\Throwable $e): Result
	{
		return new Err($e);
	}
	
	/**
	 * Wraps the traversable into an Iterator
	 *
	 * @param \Traversable|array|\ArrayObject $v
	 *
	 * @return Iterator
	 */
	public static function iter($v): Iterator
	{
		return new Iterator($v);
	}
}
