<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Generator;

use Apk\Fitter\ConsumerTrait;
use Apk\Fitter\AdaptorTrait;
use Apk\Fitter\StaticTrait;

/**
 * Class Range
 * @package Apk\Fitter\Generator
 *
 * A generator that will create a new number for each iteration.
 */
class Range implements \Iterator
{
	use StaticTrait;
	use AdaptorTrait;
	use ConsumerTrait;
	
	protected $startValue = 0;
	protected $endValue = PHP_INT_MAX;
	protected $stepValue = 1;
	protected $currentValue = 0;
	protected $index = 0;
	
	/**
	 * Range constructor.
	 *
	 * @param int $start First number to return
	 * @param int $end   Upper bound on the generated number (could not be the last number returned due to step)
	 * @param int $step  The amount to add at each iteration
	 */
	public function __construct($start = 0, $end = PHP_INT_MAX, $step = 1)
	{
		if (!is_numeric($start) || !is_numeric($end) || !is_numeric($step)) {
			throw new \UnexpectedValueException('Start, end and step must be numeric');
		}
		
		if ($step > 0 && $end < $start) {
			throw new \UnexpectedValueException('Step cannot be positive for a reverse range');
		} else if ($step < 0 && $end > $start) {
			throw new \UnexpectedValueException('Step cannot be negative for a growing range');
		}
		
		$this->startValue = $start;
		$this->endValue = $end;
		$this->stepValue = $step;
		
		$this->currentValue = $start;
	}
	
	/**
	 * @return mixed
	 */
	public function current()
	{
		return $this->currentValue;
	}
	
	public function next()
	{
		$this->currentValue += $this->stepValue;
		$this->index++;
	}
	
	/**
	 * @return mixed
	 */
	public function key()
	{
		return $this->index;
	}
	
	/**
	 * @return bool
	 */
	public function valid()
	{
		return ($this->currentValue <= $this->endValue);
	}
	
	public function rewind()
	{
		$this->currentValue = $this->startValue;
		$this->index = 0;
	}
}
