<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Generator;

use Apk\Fitter\ConsumerTrait;
use Apk\Fitter\AdaptorTrait;
use Apk\Fitter\StaticTrait;

/**
 * Class StringWords
 * @package Apk\Fitter\Generator
 *
 * This iterator will split a string into its words and return one each iteration.
 */
class StringWords implements \Iterator
{
	use StaticTrait;
	use AdaptorTrait;
	use ConsumerTrait;
	
	protected $string = '';
	protected $wordpos = 0;
	protected $pos = 0;
	protected $lastPos;
	protected $found = '';
	protected static $BOUNDARIES = [
		" ",
		"\n",
		"\r",
		"\t",
		"\xc2\xa0",
		"\xe2\x80\x80",
		"\xe2\x80\x81",
		"\xe2\x80\x82",
		"\xe2\x80\x83",
		"\xe2\x80\x84",
		"\xe2\x80\x85",
		"\xe2\x80\x86",
		"\xe2\x80\x87",
		"\xe2\x80\x88",
		"\xe2\x80\x89",
		"\xe2\x80\x8a",
		"\xe2\x80\x8b",
		"\xe2\x80\xaf",
		"\xe2\x81\x9f",
		"\xe3\x80\x80",
		"\xef\xbb\xbf"
	];
	
	/**
	 * StringWords constructor.
	 *
	 * @param string $string  The string to split
	 * @param string $charset The charset the string is encoded in
	 */
	public function __construct($string, $charset = 'UTF-8')
	{
		if (!is_string($string)) {
			throw new \UnexpectedValueException('Value to process must be a string');
		}
		
		if ($charset != 'UTF-8') {
			$string = mb_convert_encoding($string, 'UTF-8', $charset);
		}
		
		$this->string = $string;
		$this->lastPos = mb_strlen($string);
		$this->pos = 0;
	}
	
	/**
	 * @return mixed
	 */
	public function current()
	{
		return $this->found;
	}
	
	public function next()
	{
		$this->found = '';
		$this->wordpos = $this->pos;
		while ($this->pos < $this->lastPos) {
			$ch = mb_substr($this->string, $this->pos, 1);
			$this->pos++;
			
			if (in_array($ch, self::$BOUNDARIES)) {
				if ($this->found == '') {
					$this->wordpos++;
					continue;
				} else {
					return;
				}
			}
			$this->found .= $ch;
		}
	}
	
	/**
	 * @return mixed
	 */
	public function key()
	{
		return $this->wordpos;
	}
	
	/**
	 * @return bool
	 */
	public function valid()
	{
		return $this->found !== '';
	}
	
	public function rewind()
	{
		$this->pos = 0;
		$this->next();
	}
	
}
