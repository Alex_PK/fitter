<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter;

use Apk\Fitter\Types\Functor;

/**
 * Class Iterator
 * @package Apk\Iterators
 *
 * Implements the basic iterator with all the adaptor and consumer traits.
 */
class Iterator extends BaseIterator implements Functor
{
	use StaticTrait;
	use AdaptorTrait;
	use ConsumerTrait;
}
