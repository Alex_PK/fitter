<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter;

trait StaticTrait
{
	/**
	 * Allows to create and immediately chain functions on the created object:
	 *
	 * Iterator::create()->someFunction()...
	 *
	 * In newer versions of PHP you can do the same with
	 * (new Iterator())->someFunction()...
	 *
	 * @param mixed ...$args
	 *
	 * @return Iterator
	 */
	static public function from(...$args)
	{
		$newClass = new \ReflectionClass(get_called_class());
		
		return $newClass->newInstanceArgs($args);
	}
}
