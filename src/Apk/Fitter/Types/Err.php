<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Exception\InvalidResultException;
use Apk\Fitter\Exception\UnwrapException;
use Apk\Fitter\F;
use Apk\Fitter\Iterator;

final class Err extends Result implements Monad, Iterable
{
	protected $err;
	
	public function __construct(\Throwable $e)
	{
		$this->err = $e;
	}
	
	/** @inheritdoc */
	public function isOk(): bool { return false; }
	
	/** @inheritdoc */
	public function isErr(): bool { return true; }
	
	/** @inheritdoc */
	public function unwrap()
	{
		throw new UnwrapException('Unwrapping an error', 0, $this->err);
	}
	
	/** @inheritdoc */
	public function unwrapOr($default)
	{
		return $default;
	}
	
	/** @inheritdoc */
	public function unwrapOrElse(callable $f)
	{
		return $f($this->err);
	}
	
	/** @inheritdoc */
	public function unwrapErr()
	{
		return $this->err;
	}
	
	/** @inheritdoc */
	public function someOk(): Option
	{
		return new None();
	}
	
	/** @inheritdoc */
	public function someErr(): Option
	{
		return new Some($this->err);
	}
	
	/** @inheritdoc */
	public function map(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function mapErr(callable $f): Result
	{
		try {
			$res = $f($this->err);
		} catch (\Throwable $e) {
			$res = $e;
		}
		
		if (!$res instanceof \Throwable) {
			$res = new InvalidResultException(
				'The result of the function passed to Result::mapErr must be an Exception'
			);
		}
		
		return new Err($res);
	}
	
	/** @inheritdoc */
	public function catch (Result $value): Result
	{
		return $value;
	}
	
	/** @inheritdoc */
	public function catchThen(callable $f): Result
	{
		$res = $f($this->err);
		if (!$res instanceof Result) {
			return self::err(
				new InvalidResultException('The result of the function passed to Result::catchThen must be a Result')
			);
		}
		
		return $res;
	}
	
	/** @inheritdoc */
	public function catchThenTry(callable $f): Result
	{
		try {
			return self::ok($f($this->err));
			
		} catch (\Throwable $e) {
			return self::err($e);
		}
	}
	
	/** @inheritdoc */
	public function and (Result $value): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function andThen(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function andThenTry(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function or (Result $value): Result
	{
		return $value;
	}
	
	/** @inheritdoc */
	public function orElse(callable $f): Result
	{
		$res = $f();
		if (!$res instanceof Result) {
			$res = self::err(
				new InvalidResultException('The result of the function passed to Result::or_else must be a Result')
			);
		}
		
		return $res;
	}
	
	/** @inheritdoc */
	public function orElseTry(callable $f): Result
	{
		try {
			return self::ok($f());
			
		} catch (\Throwable $e) {
			return self::err($e);
		}
	}
	
	/** @inheritdoc */
	public function iter(): Iterator
	{
		return new Iterator([]);
	}

	/** @inheritdoc */
	public function iterBy(callable $f): Iterator
	{
		return Iterator::from([]);
	}

	/** @inheritdoc */
	public function apply(Functor $f): Monad
	{
		return $this;
	}
}
