<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */
 
namespace Apk\Fitter\Types;

interface Functor
{
	/**
	 * PHP does not (yet?) support return type covariance, so we cannot declare a type here,
	 * but this function must return an object implementing Functor
	 */
	public function map(callable $f);
}
