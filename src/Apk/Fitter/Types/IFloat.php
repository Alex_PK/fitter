<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */
 
namespace Apk\Fitter\Types;

use Apk\Fitter\Iterator;

class IFloat implements Functor, Iterable, Unwrappable
{
	/** @var float */
	private $value;

	/**
	 * @param float $value
	 */
	public function __construct(float $value)
	{
		$this->value = $value;
	}

	/**
	 * Create an IFloat containing the passed value
	 *
	 * @param float $value
	 *
	 * @return IFloat
	 */
	static public function from(float $value)
	{
		return new self($value);
	}

	/**
	 * Create an IFloat containing the value returned by the passed function
	 *
	 * @param callable $f function(): float
	 *
	 * @return IFloat
	 */
	static public function fromCalling(callable $f)
	{
		$value = $f();
		return new self($value);
	}

	/**
	 * Transform the IFloat into a Result with an Ok case, containing the same value
	 *
	 * @return Result
	 */
	public function ok(): Result
	{
		return Result::ok($this->value);
	}

	/**
	 * Transform the IFloat into an Option with a Some case, containing the same value
	 *
	 * @return Option
	 */
	public function some(): Option
	{
		return Option::some($this->value);
	}

	/**
	 * Create an Iterator with a single element containing the same value
	 *
	 * @return Iterator
	 */
	public function iter(): Iterator
	{
		return new Iterator([$this->value]);
	}

	/**
	 * Transforms the IString into an iterator by calling the function to generate the items
	 *
	 * The function must return an array or a Traversable (including a Generator)
	 *
	 * @param callable $f function(mixed $v): array|Traversable
	 *
	 * @return Iterator
	 */
	public function iterBy(callable $f)
	{
		return Iterator::from($f($this->value));
	}

	/**
	 * Retrieve the contained value
	 *
	 * @return float
	 */
	public function unwrap(): float
	{
		return $this->value;
	}

	/**
	 * Apply a function to the contained value and wrap the result in a new IFloat
	 *
	 * @param callable $f function(float $v): float
	 *
	 * @return IFloat
	 */
	public function map(callable $f): IFloat
	{
		return self::from($f($this->value));
	}

	/**
	 * Call the function on the contained value.
	 * The function must return a new IString containing the result.
	 *
	 * @param callable $f function($v): IFloat
	 *
	 * @return IFloat
	 */
	public function bind(callable $f): IFloat
	{
		return $f($this->unwrap());
	}
}
