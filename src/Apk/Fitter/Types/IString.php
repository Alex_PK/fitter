<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Generator\StringWords;
use Apk\Fitter\Iterator;

class IString implements Functor, Iterable, Unwrappable
{
	/** @var string */
	private $value;

	/**
	 * @param string $value
	 *
	 */
	public function __construct(string $value)
	{
		$this->value = $value;
	}

	/**
	 * Create an IString containing the passed value
	 *
	 * @param string $value
	 *
	 * @return IString
	 */
	static public function from(string $value)
	{
		return new self($value);
	}

	/**
	 * Create an IString containing the value returned by the passed function
	 *
	 * @param callable $f function(): string
	 *
	 * @return IString
	 */
	static public function fromCalling(callable $f)
	{
		$value = $f();

		return new self($value);
	}

	/**
	 * Transform the IString into a Result with an Ok case, containing the same value
	 *
	 * @return Result
	 */
	public function ok(): Result
	{
		return Result::ok($this->value);
	}

	/**
	 * Transform the IString into an Option with a Some case, containing the same value
	 *
	 * @return Option
	 */
	public function some(): Option
	{
		return Option::some($this->value);
	}

	/**
	 * Create an Iterator with a single element containing the same value
	 *
	 * @return Iterator
	 */
	public function iter(): Iterator
	{
		return new Iterator([$this->value]);
	}

	/**
	 * Create an Iterator to iterate over every character of the string (UTF8 format)
	 *
	 * @return Iterator
	 */
	public function chars(): Iterator
	{
		return Iterator::from(preg_split('//u', $this->value, -1, PREG_SPLIT_NO_EMPTY));
	}

	/**
	 * Create an Iterator to iterate over every word (space-separated part) of the string (UTF8 format)
	 *
	 * @return Iterator
	 */
	public function words(): Iterator
	{
		return Iterator::from(new StringWords($this->value));
	}

	/**
	 * Transforms the IString into an iterator by calling the function to generate the items
	 *
	 * The function must return an array or a Traversable (including a Generator)
	 *
	 * @param callable $f function(mixed $v): array|Traversable
	 *
	 * @return Iterator
	 */
	public function iterBy(callable $f)
	{
		return Iterator::from($f($this->value));
	}

	/**
	 * Retrieve the contained value
	 *
	 * @return string
	 */
	public function unwrap(): string
	{
		return $this->value;
	}

	/**
	 * Apply a function to the contained value and wrap the result in a new IInt
	 *
	 * @param callable $f function(string $v): string
	 *
	 * @return IString
	 */
	public function map(callable $f): IString
	{
		return self::from($f($this->value));
	}

	/**
	 * Call the function on the contained value.
	 * The function must return a new IString containing the result.
	 *
	 * @param callable $f function($v): IString
	 *
	 * @return IString
	 */
	public function bind(callable $f): IString
	{
		return $f($this->unwrap());
	}
}
