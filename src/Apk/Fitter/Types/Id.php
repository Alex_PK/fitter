<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */
 
namespace Apk\Fitter\Types;

use Apk\Fitter\Iterator;

class Id implements Monad, Iterable
{
	private $value;

	/**
	 * @param mixed $value
	 */
	public function __construct($value)
	{
		$this->value = $value;
	}

	/**
	 * Create an Id containing the passed value
	 *
	 * @param mixed $value
	 *
	 * @return Id
	 */
	static public function from($value)
	{
		return new self($value);
	}

	/**
	 * Create an Id containing the value returned by the passed function
	 *
	 * @param callable $f
	 *
	 * @return Id
	 */
	static public function fromCalling(callable $f)
	{
		$value = $f();
		return new self($value);
	}

	/**
	 * Transform the Id into a Result with an Ok case, containing the same value
	 *
	 * @return Result
	 */
	public function ok(): Result
	{
		return Result::ok($this->value);
	}

	/**
	 * Transform the Id into an Option with a Some case, containing the same value
	 *
	 * @return Option
	 */
	public function some(): Option
	{
		return Option::some($this->value);
	}

	/**
	 * Create an Iterator with a single element containing the same value
	 *
	 * @return Iterator
	 */
	public function iter(): Iterator
	{
		return new Iterator([$this->value]);
	}

	/**
	 * Transforms the IString into an iterator by calling the function to generate the items
	 *
	 * The function must return an array or a Traversable (including a Generator)
	 *
	 * @param callable $f function(mixed $v): array|Traversable
	 *
	 * @return Iterator
	 */
	public function iterBy(callable $f)
	{
		return Iterator::from($f($this->value));
	}

	/**
	 * Retrieve the contained value
	 *
	 * @return mixed
	 */
	public function unwrap()
	{
		return $this->value;
	}

	/**
	 * Apply a function to the contained value and wrap the result in a new Id
	 *
	 * @param callable $f
	 *
	 * @return Id
	 */
	public function map(callable $f): Id
	{
		return self::from($f($this->value));
	}

	/**
	 * Apply the function contained in the Id to the functor via its map()
	 *
	 * @param Functor $f
	 *
	 * @return Monad
	 */
	public function apply(Functor $f): Monad
	{
		return $f->map($this->unwrap());
	}

	/**
	 * Call the function on the contained value.
	 * The function must return a new Monad containing the result.
	 *
	 * @param callable $f function($v): Monad
	 *
	 * @return Monad
	 */
	public function bind(callable $f): Monad
	{
		return $f($this->unwrap());
	}
}
