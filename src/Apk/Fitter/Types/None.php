<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Exception\InvalidResultException;
use Apk\Fitter\Exception\UnwrapException;
use Apk\Fitter\F;
use Apk\Fitter\Iterator;

final class None extends Option implements Monad, Iterable
{
	public function __construct()
	{
	}
	
	/** @inheritdoc */
	public function isSome(): bool { return false; }
	
	/** @inheritdoc */
	public function isNone(): bool { return true; }
	
	/** @inheritdoc */
	public function unwrap()
	{
		throw new UnwrapException("Trying to unwrap a None value");
	}
	
	/** @inheritdoc */
	public function unwrapOr($default)
	{
		return $default;
	}
	
	/** @inheritdoc */
	public function unwrapOrElse(callable $f)
	{
		return $f();
	}
	
	/** @inheritdoc */
	public function map(callable $f): Option
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function mapOr($default, callable $f): Option
	{
		return self::some($default);
	}
	
	/** @inheritdoc */
	public function mapOrElse(callable $default, callable $f): Option
	{
		return self::some($default());
	}
	
	/** @inheritdoc */
	public function okOr(\Throwable $e): Result
	{
		return Result::err($e);
	}
	
	/** @inheritdoc */
	public function okOrElse(callable $f): Result
	{
		try {
			$res = $f();
		} catch (\Throwable $e) {
			$res = $e;
		}
		
		if (!$res instanceof \Throwable) {
			$res = new InvalidResultException('The result of the function called in okOrElse must be an Exception');
		}
		
		return Result::err($res);
	}
	
	/** @inheritdoc */
	public function and (Option $value): Option
	{
		return self::none();
	}
	
	/** @inheritdoc */
	public function andThen(callable $f): Option
	{
		return self::none();
	}
	
	public function andThenTry(callable $f, $catchFalse = false): Option
	{
		return self::none();
	}
	
	/** @inheritdoc */
	public function or (Option $value): Option
	{
		return $value;
	}
	
	/** @inheritdoc */
	public function orElse(callable $f): Option
	{
		$res = $f();
		if (!$res instanceof Option) {
			$res = self::none();
		}
		
		return $res;
	}
	
	/** @inheritdoc */
	public function iter(): Iterator
	{
		return new Iterator([]);
	}

	/** @inheritdoc */
	public function iterBy(callable $f): Iterator
	{
		return Iterator::from([]);
	}

	/** @inheritdoc */
	public function apply(Functor $f): Monad
	{
		return $this;
	}
}
