<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Exception\InvalidResultException;
use Apk\Fitter\Exception\UnwrapException;
use Apk\Fitter\Iterator;

final class Ok extends Result implements Monad
{
	private $value;
	
	public function __construct($value)
	{
		$this->value = $value;
	}
	
	/** @inheritdoc */
	public function isOk(): bool { return true; }
	
	/** @inheritdoc */
	public function isErr(): bool { return false; }
	
	/** @inheritdoc */
	public function unwrap()
	{
		return $this->value;
	}
	
	/** @inheritdoc */
	public function unwrapOr($default)
	{
		return $this->unwrap();
	}
	
	/** @inheritdoc */
	public function unwrapOrElse(callable $f)
	{
		return $this->unwrap();
	}
	
	/** @inheritdoc */
	public function unwrapErr()
	{
		throw new UnwrapException('Tried to unwrap an error in an Ok Result');
	}
	
	/** @inheritdoc */
	public function someOk(): Option
	{
		return new Some($this->value);
	}
	
	/** @inheritdoc */
	public function someErr(): Option
	{
		return new None();
	}
	
	/** @inheritdoc */
	public function map(callable $f): Result
	{
		return self::ok($f($this->value));
	}
	
	/** @inheritdoc */
	public function mapErr(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function catch (Result $value): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function catchThen(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function catchThenTry(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function and (Result $value): Result
	{
		return $value;
	}
	
	/** @inheritdoc */
	public function andThen(callable $f): Result
	{
		$res = $f($this->value);
		if (!$res instanceof Result) {
			$res = self::err(
				new InvalidResultException('The result of the function passed to Result::and_then must be a Result')
			);
		}
		
		return $res;
	}
	
	/** @inheritdoc */
	public function andThenTry(callable $f): Result
	{
		try {
			return self::ok($f($this->value));
			
		} catch (\Throwable $e) {
			return self::err($e);
		}
	}
	
	/** @inheritdoc */
	public function or (Result $value): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function orElse(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function orElseTry(callable $f): Result
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function iter(): Iterator
	{
		return new Iterator([$this->value]);
	}

	/** @inheritdoc */
	public function iterBy(callable $f): Iterator
	{
		return Iterator::from($f($this->value));
	}

	/** @inheritdoc */
	public function apply(Functor $f): Monad
	{
		return $f->map($this->unwrap());
	}
}
