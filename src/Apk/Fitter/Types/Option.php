<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Iterator;

abstract class Option implements Monad, Iterable
{
	/**
	 * Create an Option containing Some() with the provided value
	 *
	 * @param mixed $value
	 *
	 * @return Option
	 */
	public static function some($value): Option
	{
		return new Some($value);
	}
	
	/**
	 * Create an Option containing None
	 *
	 * @return Option
	 */
	public static function none(): Option
	{
		return new None();
	}
	
	/**
	 * Creates an Option from a generic value. If the value is null, it will return a None. Otherwise Some
	 *
	 * @param mixed $value
	 * @param bool  $catchFalse If true, also transform false into None, not just null
	 *
	 * @return Option
	 */
	public static function from($value, bool $catchFalse = false): Option
	{
		if (is_null($value) || ($catchFalse && false === $value)) {
			return self::none();
		} else {
			return self::some($value);
		}
	}
	
	/**
	 * Creates an Option from a called function. If the value is null, it will return a None. Otherwise Some
	 *
	 * @param callable $f          function(): mixed
	 * @param bool     $catchFalse If true, also transform false into None, not just null
	 *
	 * @return Option
	 */
	public static function fromCalling(callable $f, bool $catchFalse = false): Option
	{
		$value = $f();
		if (is_null($value) || ($catchFalse && false === $value)) {
			return self::none();
		} else {
			return self::some($value);
		}
	}
	
	/**
	 * Check if the option contains some value
	 *
	 * @return bool
	 */
	abstract public function isSome(): bool;
	
	/**
	 * Check if the option contains no value
	 *
	 * @return bool
	 */
	abstract public function isNone(): bool;
	
	/**
	 * Extract the value from the option if present. Throw an exception otherwise
	 *
	 * @return mixed
	 * @throws \Throwable
	 */
	abstract public function unwrap();
	
	/**
	 * Extract the value from the option if present, otherwise return the given default value
	 *
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	abstract public function unwrapOr($default);
	
	/**
	 * Extract the value from the option if present, otherwise call the given function to calculate a default value
	 *
	 * @param callable $f function(): mixed
	 *
	 * @return mixed
	 */
	abstract public function unwrapOrElse(callable $f);
	
	/**
	 * Apply the given function to the value contained in the Option (if present).
	 * The function doesn't need to wrap the result into an Option, as it will be automatically done by map.
	 * If the Option contains no value, return None
	 *
	 * @param callable $f function(mixed $value): mixed
	 *
	 * @return Option
	 */
	abstract public function map(callable $f): Option;
	
	/**
	 * Apply the given function to the value contained in the Option (if present).
	 * The function doesn't need to wrap the result into an Option, as it will be automatically done by map.
	 * If the Option contains no value, return the default given, wrapped in a new Option.
	 *
	 * @param mixed    $default
	 * @param callable $f function(mixed $value): mixed
	 *
	 * @return Option
	 */
	abstract public function mapOr($default, callable $f): Option;
	
	/**
	 * Apply the given function to the value contained in the Option (if present).
	 * The function doesn't need to wrap the result into an Option, as it will be automatically done by map.
	 * If the Option contains no value, use the given function to calculate a default value, which will be wrapped
	 * in a new Option.
	 *
	 * @param callable $default function(): mixed
	 * @param callable $f       function(mixed $value): mixed
	 *
	 * @return Option
	 */
	abstract public function mapOrElse(callable $default, callable $f): Option;
	
	/**
	 * Transform the Option into a Result, using the contained value as the Ok value, if present,
	 * or the given Exception as Err value.
	 *
	 * @param \Throwable $e
	 *
	 * @return Result
	 */
	abstract public function okOr(\Throwable $e): Result;
	
	/**
	 * Transform the Option into a Result, using the contained value as the Ok value, if present,
	 * or using the given function to generate an Exception that will be used as Err value.
	 *
	 * @param callable $f function(): \Throwable
	 *
	 * @return Result
	 */
	abstract public function okOrElse(callable $f): Result;
	
	/**
	 * Return None if the Option contains None, otherwise return the given value.
	 *
	 * @param Option $value
	 *
	 * @return Option
	 */
	abstract public function and(Option $value): Option;
	
	/**
	 * Return None if the Option contains None, otherwise call the given function, which will need to return
	 * a new Option (containing a new value or None)
	 *
	 * @param callable $f function(mixed $value): Option
	 *
	 * @return Option
	 */
	abstract public function andThen(callable $f): Option;
	
	/**
	 * Return None if the Option contains None, otherwise call the given function.
	 * If the called function returns null, returns None. Otherwise, wrap the result in a Some.
	 *
	 * This function is useful to directly call a non-Option-aware PHP function that could return null (or false).
	 *
	 * @param callable $f          function(mixed $value): ?mixed
	 * @param bool     $catchFalse If set to true, also transform false into a None
	 *
	 * @return Option
	 */
	abstract public function andThenTry(callable $f, $catchFalse = false): Option;
	
	/**
	 * Return the same Option with the same value if there is a value present, otherwise return the given value
	 *
	 * @param Option $value
	 *
	 * @return Option
	 */
	abstract public function or(Option $value): Option;
	
	/**
	 * Return the same Option with the same value if there is a value present, otherwise call the given function,
	 * which will need to return a new Option (containing a new value or None)
	 *
	 * @param callable $f function(): Option
	 *
	 * @return mixed
	 */
	abstract public function orElse(callable $f): Option;
	
	/**
	 * Transforms the Option into an iterator containing just the value if it is Some, or empty if it's None
	 *
	 * @return Iterator
	 */
	abstract public function iter(): Iterator;

	/**
	 * Transforms the Option into an iterator by calling the function if it is Some, or an empty iterator if it's None
	 *
	 * The function must return an array or a Traversable (including a Generator)
	 *
	 * @param callable $f function(mixed $v): array|Traversable
	 *
	 * @return Iterator
	 */
	abstract public function iterBy(callable $f) : Iterator;

	/**
	 * Apply the function contained in the Option to the given functor/monad.
	 * If the Option contains None, the id() function is applied, returning the same value
	 *
	 * @param Functor $f
	 *
	 * @return Monad
	 */
	abstract public function apply(Functor $f): Monad;

	public function bind(callable $f): Monad
	{
		return $this->andThen($f);
	}
}
