<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Iterator;

abstract class Result implements Monad, Iterable
{
	/**
	 * Create an Ok Result from the value
	 *
	 * @param mixed $value
	 *
	 * @return Result
	 */
	public static function ok($value): Result
	{
		return new Ok($value);
	}
	
	/**
	 * Create an Err Result from an Exception
	 *
	 * @param \Throwable $e
	 *
	 * @return Result
	 */
	public static function err(\Throwable $e): Result
	{
		return new Err($e);
	}
	
	/**
	 * Creates a Result from a generic value.
	 * if the value is an Exception, it will return an Err. Otherwise an Ok
	 *
	 * @param mixed $value
	 *
	 * @return Result
	 */
	public static function from($value): Result
	{
		if ($value instanceof \Throwable) {
			return self::err($value);
		} else {
			return self::ok($value);
		}
	}
	
	/**
	 * Creates a Result from calling a function.
	 * if the result is an Exception, it will return an Err. Otherwise an Ok
	 *
	 * @param callable $f function(): mixed throws \Throwable
	 *
	 * @return Result
	 */
	public static function fromCalling(callable $f): Result
	{
		try {
			return self::ok($f());
		} catch (\Throwable $e) {
			return self::err($e);
		}
	}
	
	/**
	 * Check if the Result contains an Ok value
	 *
	 * @return bool
	 */
	abstract public function isOk(): bool;
	
	/**
	 * Check if the Result contains an Err value
	 *
	 * @return bool
	 */
	abstract public function isErr(): bool;
	
	/**
	 * Return an Option containing the Ok value, if present, or None if the Result contains an Err
	 *
	 * @return Option
	 */
	abstract public function someOk(): Option;
	
	/**
	 * Return an Option containing the Err value, if present, or None if the Result contains an Ok value
	 *
	 * @return Option
	 */
	abstract public function someErr(): Option;
	
	/**
	 * Return the contained value if it's Ok, or throw an Exception if the Result contains an Err
	 *
	 * @return mixed
	 * @throws \Throwable
	 */
	abstract public function unwrap();
	
	/**
	 * Return the contained value if it's Ok, otherwise return the given default value.
	 *
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	abstract public function unwrapOr($default);
	
	/**
	 * Return the contained value if it's Ok, otherwise call the given function to calculate a default value.
	 *
	 * @param callable $f function(\Throwable $e): mixed
	 *
	 * @return mixed
	 */
	abstract public function unwrapOrElse(callable $f);
	
	/**
	 * Return the Err value if the Result contains an Err, otherwise throw an exception
	 *
	 * @return mixed
	 * @throws \Throwable
	 */
	abstract public function unwrapErr();
	
	/**
	 * Call the given function on the contained value if it's Ok.
	 * The function doesn't need to return a Result, as it will be automatically wrapped in a new Result as Ok.
	 * If it contains an Err, return the same Result
	 *
	 * @param callable $f function(mixed $value): mixed
	 *
	 * @return Result
	 */
	abstract public function map(callable $f): Result;
	
	/**
	 * Call the given function on the contained Err if it is Err
	 * The function should return a new Exception that will be wrapped in a new Result as Err.
	 * If it contains an Ok value, return the same result
	 *
	 * @param callable $f function(\Throwable $e) : \Exception
	 *
	 * @return Result
	 */
	abstract public function mapErr(callable $f): Result;
	
	/**
	 * Catches an Err and allows to transform it into an Ok value
	 *
	 * @param Result $value
	 *
	 * @return Result
	 */
	abstract public function catch (Result $value): Result;
	
	/**
	 * Catches an Err and allows to transform it into an Ok value by calling a function
	 *
	 * @param callable $f function(\Throwable $e): Result
	 *
	 * @return Result
	 */
	abstract public function catchThen(callable $f): Result;
	
	/**
	 * Catches an Err and allows to transform it into a new Result by calling a function
	 *
	 * @param callable $f function(\Throwable $e): mixed
	 *
	 * @return Result
	 */
	abstract public function catchThenTry(callable $f): Result;
	
	/**
	 * If the Result contains an Ok Value, return the passed value (which must be a Result).
	 * If the Result contains an Err, ignore the given value and return the Result itself
	 *
	 * @param Result $value
	 *
	 * @return Result
	 */
	abstract public function and (Result $value): Result;
	
	/**
	 * If the Result contains an Ok Value, call the function, which needs to generate a new Result, that will be
	 * returned. If the Result contains an Err, ignore the given value and return the Result itself
	 *
	 * @param callable $f function(mixed $value): Result
	 *
	 * @return Result
	 */
	abstract public function andThen(callable $f): Result;
	
	/**
	 * If the Result contains an Ok Value, call the function. If the function throws an Exception, wrap that in an Err,
	 * otherwise, wrap the result in a Ok
	 * If the Result contains an Err, ignore the given value and return the Result itself
	 *
	 * This function is useful to directly call a non-Result-aware function and wrap its return value in a Result
	 *
	 * @param callable $f function(mixed $value): mixed throws \Throwable
	 *
	 * @return Result
	 */
	abstract public function andThenTry(callable $f): Result;
	
	/**
	 * If the Result contains an Err Value, return the passed value (which must be a Result).
	 * If the Result contains an Ok, ignore the given value and return the Result itself
	 *
	 * @param Result $value
	 *
	 * @return Result
	 */
	abstract public function or (Result $value): Result;
	
	/**
	 * If the Result contains an Err Value, call the function to generate a new Result, that will be returned.
	 * If the Result contains an Ok, ignore the given value and return the Result itself
	 *
	 * @param callable $f function(\Throwable $e): Result
	 *
	 * @return Result
	 */
	abstract public function orElse(callable $f): Result;
	
	/**
	 * If the Result contains an Err Value, call the function, whiuch could generate a new value or throw.
	 * Either will be wrapped in a new Result.
	 * If the Result contains an Ok, ignore the given value and return the Result itself
	 *
	 * @param callable $f function(): mixed throws \Throwable
	 *
	 * @return Result
	 */
	abstract public function orElseTry(callable $f): Result;
	
	/**
	 * Transforms the Result into an iterator containing just the value if it is Ok, or empty if it's Err
	 *
	 * @return Iterator
	 */
	abstract public function iter(): Iterator;

	/**
	 * Transforms the Result into an iterator by calling the function if it is Ok, or an empty iterator if it's Err
	 *
	 * The function must return an array or a Traversable (including a Generator)
	 *
	 * @param callable $f function(mixed $v): array|Traversable
	 *
	 * @return Iterator
	 */
	abstract public function iterBy(callable $f) : Iterator;

	/**
	 * Apply the function contained in the result to the given functor/monad.
	 * If the Result contains Err, the id() function is applied, returning the same value
	 *
	 * @param Functor $f
	 *
	 * @return Monad
	 */
	abstract public function apply(Functor $f): Monad;
	
	public function bind(callable $f): Monad
	{
		return $this->andThen($f);
	}
}
