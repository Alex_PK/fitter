<?php
declare(strict_types=1);

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Fitter\Types;

use Apk\Fitter\Iterator;

final class Some extends Option implements Monad, Iterable
{
	private $value;
	
	public function __construct($value)
	{
		$this->value = $value;
	}
	
	/** @inheritdoc */
	public function isSome(): bool { return true; }
	
	/** @inheritdoc */
	public function isNone(): bool { return false; }
	
	/** @inheritdoc */
	public function unwrap()
	{
		return $this->value;
	}
	
	/** @inheritdoc */
	public function unwrapOr($default)
	{
		return $this->unwrap();
	}
	
	/** @inheritdoc */
	public function unwrapOrElse(callable $f)
	{
		return $this->unwrap();
	}
	
	/** @inheritdoc */
	public function map(callable $f): Option
	{
		return new Some($f($this->value));
	}
	
	/** @inheritdoc */
	public function mapOr($default, callable $f): Option
	{
		return $this->map($f);
	}
	
	/** @inheritdoc */
	public function mapOrElse(callable $default, callable $f): Option
	{
		return $this->map($f);
	}
	
	/** @inheritdoc */
	public function okOr(\Throwable $e): Result
	{
		return Result::ok($this->value);
	}
	
	/** @inheritdoc */
	public function okOrElse(callable $f): Result
	{
		return Result::ok($this->value);
	}
	
	/** @inheritdoc */
	public function and (Option $value): Option
	{
		return $value;
	}
	
	/** @inheritdoc */
	public function andThen(callable $f): Option
	{
		$res = $f($this->value);
		if (!$res instanceof Option) {
			$res = new None();
		}
		
		return $res;
	}
	
	/** @inheritdoc */
	public function andThenTry(callable $f, $catchFalse = false): Option
	{
		$res = $f($this->value);
		if (is_null($res) || ($catchFalse && false === $res)) {
			return self::none();
		} else {
			return self::some($res);
		}
	}
	
	/** @inheritdoc */
	public function or (Option $value): Option
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function orElse(callable $f): Option
	{
		return $this;
	}
	
	/** @inheritdoc */
	public function iter(): Iterator
	{
		return new Iterator([$this->value]);
	}

	/** @inheritdoc */
	public function iterBy(callable $f): Iterator
	{
		return Iterator::from($f($this->value));
	}

	/** @inheritdoc */
	public function apply(Functor $f): Monad
	{
		return $f->map($this->unwrap());
	}
}
