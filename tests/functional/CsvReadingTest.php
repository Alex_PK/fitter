<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Functional;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class CsvReadingTest extends TestCase
{
	public function testCounting()
	{
		$csvIterator = new \SplFileObject(__DIR__ . '/../assets/us-500.csv');
		$csvIterator->setFlags(
			\SplFileObject::READ_CSV |
			\SplFileObject::READ_AHEAD |
			\SplFileObject::SKIP_EMPTY |
			\SplFileObject::DROP_NEW_LINE
		);
		$csvIterator->setCsvControl(',', '"');
		
		$processingIterator = new Iterator($csvIterator);
		$count = $processingIterator
			->skip(1)
			->count()
		;
		
		$this->assertEquals(500, $count);
	}
	
	public function testFiltering()
	{
		$csvIterator = new \SplFileObject(__DIR__ . '/../assets/us-500.csv');
		$csvIterator->setFlags(\SplFileObject::READ_CSV);
		$csvIterator->setCsvControl(",", "\"");
		
		$i = 0;
		$processingIterator = new Iterator($csvIterator);
		$count = $processingIterator
			->map(
				function ($el) {
					return [
						'firstname' => $el[0],
						'lastname'  => $el[1],
						'company'   => $el[2],
						'address'   => $el[3],
						'city'      => $el[4],
						'country'   => $el[5],
						'state'     => $el[6],
						'zip'       => $el[7],
						'phone1'    => $el[8],
						'phone2'    => $el[9],
						'email'     => $el[10],
						'web'       => $el[11]
					];
				}
			)
			->filter(
				function ($el) use (&$i) {
					return (bool)filter_var($el['email'], FILTER_VALIDATE_EMAIL);
				}
			)
			->count()
		;
		
		$this->assertEquals(500, $count);
	}
}
