<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Integration;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class ChainTest extends TestCase
{
	function testBasic()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$a2 = ['a', 'e', 'o', 'u', 'i', 'q'];
		
		$filtered = $iter->chain($a2);
		$result = $filtered->toArray();
		
		$this->assertEquals(11, count($result));
		$this->assertEquals([4, 7, 2, 9, 5, 'a', 'e', 'o', 'u', 'i', 'q'], $result);
	}
}
