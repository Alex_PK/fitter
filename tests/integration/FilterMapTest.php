<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Integration;

use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Some;
use PHPUnit\Framework\TestCase;

class FilterMapTest extends TestCase
{
	function testBasic()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->filterMap(
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				};
				
				return Option::none();
			}
		);
		
		$this->assertEquals(
			[14, 18, 10],
			$filtered->toArray()
		);
	}
	
}
