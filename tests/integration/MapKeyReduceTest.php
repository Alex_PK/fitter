<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Integration;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class MapKeyReduceTest extends TestCase
{
	protected $sourceData = [
		[
			'name' => 'Alex',
			'age'  => 9
		],
		
		[
			'name' => 'Barbara',
			'age'  => 8
		],
		[
			'name' => 'Charlie',
			'age'  => 10
		],
		[
			'name' => 'David',
			'age'  => 10
		],
		[
			'name' => 'Erika',
			'age'  => 8
		],
		[
			'name' => 'Fran',
			'age'  => 7
		],
		[
			'name' => 'George',
			'age'  => 7
		],
		[
			'name' => 'Haley',
			'age'  => 9
		],
		[
			'name' => 'Iris',
			'age'  => 10
		],
		[
			'name' => 'Jack',
			'age'  => 7
		],
		[
			'name' => 'Keira',
			'age'  => 11
		],
		[
			'name' => 'Luke',
			'age'  => 9
		],
		[
			'name' => 'Max',
			'age'  => 10
		],
		[
			'name' => 'Nadine',
			'age'  => 7
		],
		[
			'name' => 'Otis',
			'age'  => 8
		],
		[
			'name' => 'Patrick',
			'age'  => 8
		]
	];
	
	function testBasic()
	{
		$iter = new Iterator($this->sourceData);
		
		$filtered = $iter
			->mapKey(
				function ($el) {
					return [$el['age'], $el];
				}
			)
			->reduce(
				function ($el, $prev) {
					if (is_null($prev)) {
						$prev = 0;
					}
					
					return $prev + 1;
				}
			)
		;
		
		$this->assertEquals(
			[
				9  => 3,
				8  => 4,
				10 => 4,
				7  => 4,
				11 => 1
			],
			$filtered->toArray()
		);
	}
	
	function testDumbMap()
	{
		$iter = new Iterator($this->sourceData);
		
		$filtered = $iter
			->mapKey(
				function ($el) {
					return [0, $el];
				}
			)
			->reduce(
				function ($el, $prev) {
					if (is_null($prev)) {
						$prev = 0;
					}
					
					return $prev + $el['age'];
				}
			)
		;
		
		$expected = array_reduce($this->sourceData, function ($carry, $item) { return $carry + $item['age']; }, 0);
		
		$this->assertArrayHasKey(0, $filtered);
		$this->assertEquals($expected, $filtered[0]);
	}
	
	function testInvalidMapFunction()
	{
		self::expectException(\TypeError::class);

		$iter = new Iterator($this->sourceData);
		$filtered =
			$iter->mapKey('invalid')
				->toArray()
		;
	}
	
	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testInvalidMapFunctionResult()
	{
		$iter = new Iterator($this->sourceData);
		$filtered =
			$iter->mapKey(function ($el) { return 'nothing'; })
				->toArray()
		;
	}
	
	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testInvalidMapFunctionResultKey()
	{
		$iter = new Iterator($this->sourceData);
		$filtered =
			$iter->mapKey(function ($el) { return [[1, 2], $el]; })
				->toArray()
		;
	}
	
	function testInvalidReduceFunction()
	{
		self::expectException(\TypeError::class);

		$iter = new Iterator($this->sourceData);
		$filtered = $iter->reduce('invalid');
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidReduceFunctionArrayObject()
	{
		$iter = new Iterator($this->sourceData);
		
		$arrayObject = 'A string';
		
		$filtered = $iter->reduce(function ($el, $prev) { return $el; }, $arrayObject);
	}
	
}
