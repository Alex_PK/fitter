<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Integration;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class SkipTest extends TestCase
{
	function testBasic()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			[9, 5],
			$filtered->toArray()
		);
	}
	
	function testMultipleCalls()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			[9, 5],
			$filtered->toArray()
		);
		
		$this->assertEquals(
			[9, 5],
			$filtered->toArray()
		);
	}
	
	function testMin()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			5,
			$filtered->min()
		);
	}
	
	function testMax()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			9,
			$filtered->max()
		);
	}
	
	function testAvg()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			7,
			$filtered->avg(),
			'',
			0.0001
		);
	}
	
	function testFind()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			9,
			$filtered->find(
				function ($el) {
					return $el > 5;
				}
			)
		);
	}
	
	function testFold()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip(3);
		
		$this->assertEquals(
			9 + 5,
			$filtered->fold(
				0,
				function ($el, $temp) {
					return $temp + $el;
				}
			)
		);
	}
	
	function testInvalidFunction()
	{
		self::expectException(\TypeError::class);

		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->skip('not a number');
		
		$this->assertEquals(
			[4, 7, 2, 9, 5],
			$filtered->toArray()
		);
	}
}
