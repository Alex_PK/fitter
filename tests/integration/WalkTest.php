<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Integration;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class WalkTest extends TestCase
{
	function testBasic()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map(
			function ($el) {
				return $el + 1;
			}
		);
		
		$this->assertEquals(
			[5, 8, 3, 10, 6],
			$filtered->toArray()
		);
	}
	
	function testMin()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map(
			function ($el) {
				return $el + 5;
			}
		);
		
		$this->assertEquals(
			7,
			$filtered->min()
		);
	}
	
	function testMax()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map(
			function ($el) {
				return $el + 7;
			}
		);
		
		$this->assertEquals(
			16,
			$filtered->max()
		);
	}
	
	function testAvg()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map(
			function ($el) {
				return $el + 3;
			}
		);
		
		$this->assertEquals(
			8.4,
			$filtered->avg(),
			'',
			0.0001
		);
	}
	
	function testFind()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map(
			function ($el) {
				return $el - 1;
			}
		);
		
		$this->assertEquals(
			1,
			$filtered->find(
				function ($el) {
					return $el < 2;
				}
			)
		);
	}
	
	function testFold()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map(
			function ($el) {
				return $el + 5;
			}
		);
		
		$this->assertEquals(
			4 + 7 + 2 + 9 + 5 + (5 * 5),
			$filtered->fold(
				0,
				function ($el, $temp) {
					return $temp + $el;
				}
			)
		);
	}
	
	function testInvalidFunction()
	{
		self::expectException(\TypeError::class);

		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->map('invalid');
		
		$this->assertEquals(
			[7, 9, 5],
			$filtered->toArray()
		);
	}
}
