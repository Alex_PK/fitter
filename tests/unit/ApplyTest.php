<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class ApplyTest extends TestCase
{
	public function testApply()
	{
		$limitSize = function ($image) { return $image; };
		$thumbnail = function ($image) { return $image . '_tn'; };
		$mobile = function ($image) { return $image . '_small'; };

		$images = Iterator::from(['one', 'two', 'three']);
		$process = Iterator::from([$limitSize, $thumbnail, $mobile]);

		$result = $process
			->apply($images);

		$expected = [
			'one',
			'two',
			'three',
			'one_tn',
			'two_tn',
			'three_tn',
			'one_small',
			'two_small',
			'three_small',
		];

		self::assertEquals(
			$expected,
			$result->toArray()
		);

		$grouped = $result
			->group(
				function ($el, $index) {
					return $index % 3;
				}
			);

		$expected = [
			[
				'one',
				'one_tn',
				'one_small',
			],
			[
				'two',
				'two_tn',
				'two_small',
			],
			[
				'three',
				'three_tn',
				'three_small',
			],
		];

		self::assertEquals(
			$expected,
			$grouped
		);
	}
}