<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class AvgTest extends TestCase
{
	function testAvgInt()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		
		$this->assertEquals(5.4, $iter->avg(), '', 0.0001);
	}
	
	function testInvalidFunction()
	{
		self::expectException(\TypeError::class);

		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->avg('invalid');
	}
	
	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testInvalidFunctionReturnValue()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->avg(
			function ($el) {
				return 'a' . $el;
			}
		);
	}
	
}
