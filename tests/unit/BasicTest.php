<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{
	function testCreateStatic()
	{
		$iter = Iterator::from([1, 2, 3, 4, 5, 6]);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}
		
		$this->assertEquals(1 + 2 + 3 + 4 + 5 + 6, $result);
	}
	
	function testCreateStaticAndFilter()
	{
		$iter = Iterator::from([1, 2, 3, 4, 5, 6])
			->filter(
				function ($el) {
					return $el > 3;
				}
			)
		;
		
		$this->assertEquals(4, $iter->min());
	}
	
}
