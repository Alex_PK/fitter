<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Adaptor\Chain;
use PHPUnit\Framework\TestCase;

class ChainTest extends TestCase
{
	function testBasic()
	{
		$a1 = [4, 7, 2, 9, 5];
		$a2 = ['a', 'e', 'o', 'u', 'i', 'q'];
		
		$filtered = new Chain($a1, $a2);
		$result = $filtered->toArray();
		
		$this->assertEquals(11, count($result));
		$this->assertEquals([4, 7, 2, 9, 5, 'a', 'e', 'o', 'u', 'i', 'q'], $result);
	}
	
	function testMulti()
	{
		$a1 = [4, 7, 2, 9, 5];
		$a2 = ['a', 'e', 'o', 'u', 'i', 'q'];
		$a3 = [1.2, 2.3, 3.4, 4.5, 5.6];
		
		$filtered = new Chain($a1, $a2, $a3);
		$result = $filtered->toArray();
		
		$this->assertEquals(16, count($result));
		$this->assertEquals([4, 7, 2, 9, 5, 'a', 'e', 'o', 'u', 'i', 'q', 1.2, 2.3, 3.4, 4.5, 5.6], $result);
	}
	
	function testRewind()
	{
		$a1 = [4, 7, 2, 9, 5];
		$a2 = ['a', 'e', 'o', 'u', 'i', 'q'];
		
		$filtered = new Chain($a1, $a2);
		$result = $filtered->toArray();
		
		$this->assertEquals(11, count($result));
		$this->assertEquals([4, 7, 2, 9, 5, 'a', 'e', 'o', 'u', 'i', 'q'], $result);

		$result = $filtered->toArray();
		
		$this->assertEquals(11, count($result));
		$this->assertEquals([4, 7, 2, 9, 5, 'a', 'e', 'o', 'u', 'i', 'q'], $result);
	}
	
	function testNonIteratorThrowsException()
	{
		self::expectException(\InvalidArgumentException::class);
		$chain = new Chain([1,2,3], 'a');
	}
}
