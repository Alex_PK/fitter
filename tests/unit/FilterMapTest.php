<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Adaptor\FilterMap;
use Apk\Fitter\Types\Option;
use PHPUnit\Framework\TestCase;

class FilterMapTest extends TestCase
{
	function testBasic()
	{
		$filtered = new FilterMap(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			[14, 18, 10],
			$filtered->toArray()
		);
	}
	
	function testBasicStatic()
	{
		$filtered = FilterMap::from(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			[14, 18, 10],
			$filtered->toArray()
		);
	}
	
	function testMin()
	{
		$filtered = new FilterMap(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			10,
			$filtered->min()
		);
	}
	
	function testMax()
	{
		$filtered = new FilterMap(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			18,
			$filtered->max()
		);
	}
	
	function testAvg()
	{
		$filtered = new FilterMap(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			14,
			$filtered->avg(),
			'',
			0.0001
		);
	}
	
	function testFind()
	{
		$filtered = new FilterMap(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			18,
			$filtered->find(
				function ($el) {
					return $el == 18;
				}
			)
		);
	}
	
	function testFold()
	{
		$filtered = new FilterMap(
			[4, 7, 2, 9, 5],
			function ($el) {
				if ($el > 4) {
					return Option::some($el * 2);
				} else {
					return Option::none();
				}
			}
		);
		
		$this->assertEquals(
			14 + 18 + 10,
			$filtered->fold(
				0,
				function ($el, $temp) {
					return $temp + $el;
				}
			)
		);
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$filtered = new FilterMap([4, 7, 2, 9, 5], 'invalid');
		
		$filtered->toArray();
	}
}
