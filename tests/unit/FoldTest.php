<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Iterator;
use PHPUnit\Framework\TestCase;

class FoldTest extends TestCase
{
	function testFold()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		
		$this->assertEquals(4 + 7 + 2 + 9 + 5, $iter->fold(0, function ($el, $partial) { return $partial + $el; }));
		$this->assertEquals(4 * 7 * 2 * 9 * 5, $iter->fold(1, function ($el, $partial) { return $partial * $el; }));
		$this->assertEquals(0, $iter->fold(0, function ($el, $partial) { return $partial * $el; }));
		$this->assertEquals('47295', $iter->fold('', function ($el, $partial) { return $partial . $el; }));
	}
	
	function testInvalidFunction()
	{
		self::expectException(\TypeError::class);

		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->fold(0, 'invalid');
	}
	
}
