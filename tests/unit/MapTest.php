<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Adaptor\Map;
use PHPUnit\Framework\TestCase;

class MapTest extends TestCase
{
	function testBasic()
	{
		$result = 0;
		
		$filtered = new Map(
			[4, 7, 2, 9, 5], function ($el) use (&$result) {
			$result += $el;
		}
		);
		$filtered->run();
		
		$this->assertEquals(4 + 7 + 2 + 9 + 5, $result);
	}
	
	function testBasicStatic()
	{
		$result = 0;
		
		Map::from(
			[4, 7, 2, 9, 5],
			function ($el) use (&$result) {
				$result += $el;
			}
		)
			->run()
		;
		
		$this->assertEquals(4 + 7 + 2 + 9 + 5, $result);
	}
	
}
