<?php

/**
 * This file is part of apk/fitter
 *
 * (c) Copyright 2015-2017 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Fitter\Generator\Range;
use PHPUnit\Framework\TestCase;

class RangeTest extends TestCase
{
	function testStaticCreate()
	{
		$iter = Range::from(0, 10);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(1+2+3+4+5+6+7+8+9+10, $result);

	}

	function testInt()
	{
		$iter = new Range(0, 10);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(1+2+3+4+5+6+7+8+9+10, $result);
	}

	function testIntWrongStep()
	{
		self::expectException(\UnexpectedValueException::class);
		$iter = new Range(0, -2);
	}

	function testIntWrongStepReverse()
	{
		self::expectException(\UnexpectedValueException::class);
		$iter = new Range(0, 2, -1);
	}

	function testNonNumericValues()
	{
		self::expectException(\UnexpectedValueException::class);
		$iter = new Range(0, 2, 'a');
	}

	function testIntStartNotZero()
	{
		$iter = new Range(5, 10);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(5+6+7+8+9+10, $result);
	}

	function testIntNegative()
	{
		$iter = new Range(-10, 0);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(-1-2-3-4-5-6-7-8-9-10, $result);
	}

	function testFloat()
	{
		$iter = new Range(0.0, 10.0, 0.2);
		$steps = 0;
		foreach ($iter as $num) {
			$steps++;
		}

		$this->assertEquals(51, $steps);
	}

	function testFloatStartNotZero()
	{
		$iter = new Range(2.0, 10.0, 0.2);
		$steps = 0;
		foreach ($iter as $num) {
			$steps++;
		}

		$this->assertEquals(41, $steps);
	}

	function testFloatNegative()
	{
		$iter = new Range(-10.0, 0.0, 0.2);
		$steps = 0;
		foreach ($iter as $num) {
			$steps++;
		}

		$this->assertEquals(51, $steps);
	}

	function testFloatWrongStep()
	{
		self::expectException(\UnexpectedValueException::class);
		$iter = new Range(0.0, 2.0, -0.1);
	}

	function testFloatWrongStepReverse()
	{
		self::expectException(\UnexpectedValueException::class);
		$iter = new Range(0.0, -2.0, 0.1);
	}
	
	function testKeys()
	{
		$iter = new Range(3, 12, 2);
		$i = 0;
		foreach ($iter as $k => $v) {
			self::assertEquals($i, $k);
			$i++;
		}

		$i = 0;
		foreach ($iter as $k => $v) {
			self::assertEquals($i, $k);
			$i++;
		}
	}

}
