<?php

namespace IteratorsTests\Unit\Types;

use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Functor;
use Apk\Fitter\Types\IFloat;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Result;
use PHPUnit\Framework\TestCase;

class IFloatTest extends TestCase
{
	public function testFromCalling()
	{
		$a = IFloat::fromCalling(function () { return 123.4; });

		self::assertInstanceOf(IFloat::class, $a);
		self::assertEquals(123.4, $a->unwrap(), '', 0.000001);
	}

	public function testMap()
	{
		$a = IFloat::from(123.4);

		self::assertInstanceOf(
			Functor::class,
			$a->map(function ($v) { return $v * 2; })
		);

		self::assertEquals(
			246.8,
			$a->map(function ($v) { return $v * 2; })
				->unwrap(),
			'',
			0.000001
		);

		self::assertEquals(
			123.4,
			$a->unwrap()
		);
	}

	public function testToOption()
	{
		$a = IFloat::from(123.4);

		$option = $a->some();

		self::assertInstanceOf(Option::class, $option);
		self::assertTrue($option->isSome());
		self::assertEquals(123.4, $option->unwrap());
	}

	public function testToResult()
	{
		$a = IFloat::from(123.4);

		$result = $a->ok();

		self::assertInstanceOf(Result::class, $result);
		self::assertTrue($result->isOk());
		self::assertEquals(123.4, $result->unwrap());
	}

	public function testToIter()
	{
		$a = IFloat::from(123.4);

		$iter = $a->iter();

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals([123.4], $iter->toArray());
	}

	public function testIterBy()
	{
		$a = IFloat::from(123.5);

		$iter = $a->iterBy(
			function ($v) {
				yield $v / 4;
				yield $v / 2;
				yield $v;
			}
		);

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals([30.875, 61.75, 123.5], $iter->toArray(), '', 0.000001);
	}

	public function testBind()
	{
		$a = IFloat::from(123.4);

		$boundA = $a->bind(function ($v) { return IFloat::from($v * 2); });

		self::assertInstanceOf(IFloat::class, $boundA);
		self::assertEquals(246.8, $boundA->unwrap());
	}
}
