<?php

namespace IteratorsTests\Unit\Types;

use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Functor;
use Apk\Fitter\Types\IInt;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Result;
use PHPUnit\Framework\TestCase;

class IIntTest extends TestCase
{
	public function testFromCalling()
	{
		$a = IInt::fromCalling(function () { return 123; });

		self::assertInstanceOf(IInt::class, $a);
		self::assertEquals(123, $a->unwrap());
	}

	public function testMap()
	{
		$a = IInt::from(123);
		
		self::assertInstanceOf(
			Functor::class,
			$a->map(function ($v) { return $v * 2; })
		);
		
		self::assertEquals(
			246,
			$a->map(function ($v) { return $v * 2; })
				->unwrap()
		);

        self::assertEquals(
            123,
            $a->unwrap()
        );
	}

	public function testToOption()
    {
        $a = IInt::from(123);

        $option = $a->some();

        self::assertInstanceOf(Option::class, $option);
        self::assertTrue($option->isSome());
        self::assertEquals(123, $option->unwrap());
    }

    public function testToResult()
    {
        $a = IInt::from(123);

        $result = $a->ok();

        self::assertInstanceOf(Result::class, $result);
        self::assertTrue($result->isOk());
        self::assertEquals(123, $result->unwrap());
    }

    public function testToIter()
    {
        $a = IInt::from(123);

        $iter = $a->iter();

        self::assertInstanceOf(Iterator::class, $iter);
        self::assertEquals([123], $iter->toArray());
    }

	public function testIterBy()
	{
		$a = IInt::from(120);

		$iter = $a->iterBy(
			function ($v) {
				yield $v / 4;
				yield $v / 2;
				yield $v;
			}
		);

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals([30, 60, 120], $iter->toArray());
	}

	public function testBind()
	{
		$a = IInt::from(123);

		$boundA = $a->bind(function ($v) { return IInt::from($v * 2); });

		self::assertInstanceOf(IInt::class, $boundA);
		self::assertEquals(246, $boundA->unwrap());
	}
}
