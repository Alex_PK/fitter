<?php

namespace IteratorsTests\Unit\Types;

use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Functor;
use Apk\Fitter\Types\IString;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Result;
use PHPUnit\Framework\TestCase;

class IStringTest extends TestCase
{
	public function testFromCalling()
	{
		$a = IString::fromCalling(function () { return 'A string'; });

		self::assertInstanceOf(IString::class, $a);
		self::assertEquals('A string', $a->unwrap());
	}

	public function testMap()
	{
		$a = IString::from('A string');

		self::assertInstanceOf(
			Functor::class,
			$a->map(function ($v) { return $v . $v; })
		);

		self::assertEquals(
			'A stringA string',
			$a->map(function ($v) { return $v . $v; })
				->unwrap()
		);

		self::assertEquals(
			'A string',
			$a->unwrap()
		);
	}

	public function testToOption()
	{
		$a = IString::from('A string');

		$option = $a->some();

		self::assertInstanceOf(Option::class, $option);
		self::assertTrue($option->isSome());
		self::assertEquals('A string', $option->unwrap());
	}

	public function testToResult()
	{
		$a = IString::from('A string');

		$result = $a->ok();

		self::assertInstanceOf(Result::class, $result);
		self::assertTrue($result->isOk());
		self::assertEquals('A string', $result->unwrap());
	}

	public function testToIter()
	{
		$a = IString::from('A string');

		$iter = $a->iter();

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals(['A string'], $iter->toArray());
	}

	public function testChars()
	{
		$a = IString::from('A string');
		$iter = $a->chars();

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals(['A', ' ', 's', 't', 'r', 'i', 'n', 'g'], $iter->toArray());
	}

	public function testWords()
	{
		$a = IString::from('A string with a few words');
		$iter = $a->words();

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals(['A', 'string', 'with', 'a', 'few', 'words',], $iter->toArray());
	}

	public function testIterFrom()
	{
		$a = IString::from('A string with a few words');
		$iter = $a->iterBy(
			function ($v) {
				return str_split($v, 5);
			}
		);

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals(['A str', 'ing w', 'ith a', ' few ', 'words'], $iter->toArray());
	}

	public function testIterFromGenerator()
	{
		$a = IString::from('A string');
		$iter = $a->iterBy(
			function ($v) {
				foreach (str_split($v) as $c) {
					yield $c;
				}
			}
		);

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals(['A', ' ', 's', 't', 'r', 'i', 'n', 'g'], $iter->toArray());
	}

	public function testBind()
	{
		$a = IString::from('A string');

		$boundA = $a->bind(function ($v) { return IString::from($v . $v); });

		self::assertInstanceOf(IString::class, $boundA);
		self::assertEquals('A stringA string', $boundA->unwrap());
	}
}
