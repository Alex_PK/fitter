<?php

namespace IteratorsTests\Unit\Types;

use Apk\Fitter\F;
use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Functor;
use Apk\Fitter\Types\Id;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Result;
use PHPUnit\Framework\TestCase;

class IdTest extends TestCase
{
	public function testFromCalling()
	{
		$a = Id::fromCalling(function () { return 123; });

		self::assertInstanceOf(Id::class, $a);
		self::assertEquals(123, $a->unwrap());
	}

	public function testMap()
	{
		$a = Id::from(123);

		self::assertInstanceOf(
			Functor::class,
			$a->map(function ($v) { return $v * 2; })
		);

		self::assertEquals(
			246,
			$a->map(function ($v) { return $v * 2; })
				->unwrap()
		);
	}

	public function testApplicable()
	{
		$fn = function ($v) { return $v * 2; };
		$a = Id::from(123);
		$f = Id::from($fn);

		self::assertInstanceOf(
			Id::class,
			$f->apply($a)
		);

		self::assertEquals(
			246,
			$f->apply($a)
				->unwrap()
		);

		// Applicative map law
		self::assertEquals(
			$f->apply($a),
			$a->map($fn)
		);

		// Applicative identity law
		self::assertEquals(
			Id::from([F::class, 'identity'])
				->apply($a),
			F::identity($a)
		);

		// Applicative homomorphism law
		self::assertEquals(
			Id::from($fn)
				->apply($a),
			Id::from($fn(123))
		);

		// Applicative interchange law
		self::assertEquals(
			$f->apply($a),
			$f->from(function ($f) { return $f(123); })
				->apply($f)
		);

		// Applicative composition law
		// It needs a second function to test
		$f2 = Id::from(function ($x) { return $x + 5; });

		self::assertEquals(
			$f->from([F::class, 'compose'])
				->apply($f)
				->apply($f2)
				->apply($a),
			$f->apply($f2->apply($a))
		);
	}

	public function testToOption()
	{
		$a = Id::from('A string');

		$option = $a->some();

		self::assertInstanceOf(Option::class, $option);
		self::assertTrue($option->isSome());
		self::assertEquals('A string', $option->unwrap());
	}

	public function testToResult()
	{
		$a = Id::from('A string');

		$result = $a->ok();

		self::assertInstanceOf(Result::class, $result);
		self::assertTrue($result->isOk());
		self::assertEquals('A string', $result->unwrap());
	}

	public function testToIter()
	{
		$a = Id::from('A string');

		$iter = $a->iter();

		self::assertInstanceOf(Iterator::class, $iter);
		self::assertEquals(['A string'], $iter->toArray());
	}

	public function testIterBy()
	{
		$a = Id::from(10);

		$iterA = $a->iterBy(
			function ($v) {
				for ($i = 0; $i < $v; $i++) {
					yield $i;
				}
			}
		);

		self::assertEquals([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], $iterA->toArray());
	}

	public function testBind()
	{
		$a = Id::from(123);

		$boundA = $a->bind(function ($v) { return Id::from($v * 2); });

		self::assertInstanceOf(Id::class, $boundA);
		self::assertEquals(246, $boundA->unwrap());
	}

	public function testWrongBind()
	{
		$a = Id::from(123);

		self::expectException(\Throwable::class);
		$boundA = $a->bind(function ($v) { return $v * 2; });
	}

	public function testMonadProperties()
	{
		$value = 10;
		$monad = Id::from(20);
		$fn1 = function ($v) { return Id::from($v + 10); };
		$fn2 = function ($v) { return Id::from($v * 2); };

		// Left identity
		self::assertEquals(
			$monad->from($value)
				->bind($fn1),
			$fn1($value)
		);

		// Right identity
		self::assertEquals($monad->bind([$monad, 'from']), $monad);

		// Associativity
		self::assertEquals(
			$monad->bind($fn1)
				->bind($fn2),
			$monad->bind(
				function ($x) use ($fn1, $fn2) { return $fn1($x)->bind($fn2); }
			)
		);
	}
}
