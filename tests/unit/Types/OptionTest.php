<?php

namespace IteratorsTests\Unit\Types;

use Apk\Fitter\Exception\InvalidResultException;
use Apk\Fitter\Exception\UnwrapException;
use Apk\Fitter\F;
use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Option;
use Apk\Fitter\Types\Result;
use Apk\Fitter\Types\Some;

class OptionTest extends \PHPUnit\Framework\TestCase
{
	public function testSome()
	{
		$a = Option::some(123);
		self::assertTrue($a->isSome());
		self::assertFalse($a->isNone());
		self::assertEquals(123, $a->unwrap());
	}

	public function testNone()
	{
		$a = Option::none();
		self::assertTrue($a->isNone());
		self::assertFalse($a->isSome());

		self::expectException(UnwrapException::class);
		$a->unwrap();
	}

	public function testFrom()
	{
		$a = Option::from(123);
		$b = Option::from(null);
		$c = Option::from(false, true);

		self::assertTrue($a->isSome());
		self::assertTrue($b->isNone());
		self::assertTrue($c->isNone());
	}

	public function testFromCalling()
	{
		$a = Option::fromCalling(function () { return 123; });
		$b = Option::fromCalling(function () { return null; });
		$c = Option::fromCalling(function () { return false; }, true);

		self::assertTrue($a->isSome());
		self::assertEquals(123, $a->unwrap());
		self::assertTrue($b->isNone());
		self::assertTrue($c->isNone());
	}

	public function testUnwrapOr()
	{
		$a = Option::some(123);
		$b = Option::none();

		self::assertEquals(123, $a->unwrapOr(456));
		self::assertEquals(456, $b->unwrapOr(456));
	}

	public function testUnwrapOrElse()
	{
		$a = Option::some(123);
		$b = Option::none();

		self::assertEquals(
			123,
			$a->unwrapOrElse(function () { return 456; })
		);
		self::assertEquals(
			456,
			$b->unwrapOrElse(function () { return 456; })
		);
	}

	public function testMap()
	{
		$a = Option::some(123);
		$b = Option::none();

		self::assertTrue(
			$a->map(function ($v) { return $v * 2; })
				->isSome()
		);
		self::assertFalse(
			$a->map(function ($v) { return $v * 2; })
				->isNone()
		);

		self::assertEquals(
			246,
			$a->map(function ($v) { return $v * 2; })
				->unwrap()
		);

		self::assertTrue(
			$b->map(function ($v) { return $v * 2; })
				->isNone()
		);
		self::assertFalse(
			$b->map(function ($v) { return $v * 2; })
				->isSome()
		);
	}

	public function testMapOr()
	{
		$a = Option::some(123);
		$b = Option::none();

		$mappedA = $a->mapOr(
			456,
			function ($v) { return $v + 1; }
		);

		$mappedB = $b->mapOr(
			456,
			function ($v) { return $v + 1; }
		);

		self::assertInstanceOf(Some::class, $mappedA);
		self::assertEquals(124, $mappedA->unwrap());

		self::assertInstanceOf(Some::class, $mappedB);
		self::assertEquals(456, $mappedB->unwrap());
	}

	public function testMapOrElse()
	{
		$a = Option::some(123);
		$b = Option::none();

		$mappedA = $a->mapOrElse(
			function () { return 456; },
			function ($v) { return $v + 1; }
		);

		$mappedB = $b->mapOrElse(
			function () { return 456; },
			function ($v) { return $v + 1; }
		);

		self::assertInstanceOf(Some::class, $mappedA);
		self::assertEquals(124, $mappedA->unwrap());

		self::assertInstanceOf(Some::class, $mappedB);
		self::assertEquals(456, $mappedB->unwrap());
	}

	public function testOkOr()
	{
		$a = Option::some(123);
		$b = Option::none();

		$resultA = $a->okOr(new \RuntimeException('default err'));
		$resultB = $b->okOr(new \RuntimeException('default err'));

		self::assertInstanceOf(Result::class, $resultA);
		self::assertInstanceOf(Result::class, $resultB);

		self::assertTrue($resultA->isOk());
		self::assertFalse($resultA->isErr());

		self::assertFalse($resultB->isOk());
		self::assertTrue($resultB->isErr());

		self::assertEquals(123, $resultA->unwrap());
	}

	public function testOkOrThrowsUnwrapException()
	{
		$b = Option::none();
		$resultB = $b->okOr(new \RuntimeException('default err'));

		self::expectException(UnwrapException::class);
		$resultB->unwrap();
	}

	public function testOkOrElse()
	{
		$a = Option::some(123);
		$b = Option::none();

		$resultA = $a->okOrElse(function () { return new \RuntimeException('default err'); });
		$resultB = $b->okOrElse(function () { return new \RuntimeException('default err'); });
		$resultB2 = $b->okOrElse(function () { throw new \RuntimeException('default err'); });
		$resultB3 = $b->okOrElse(function () { return 'Everything ok'; });

		self::assertInstanceOf(Result::class, $resultA);
		self::assertInstanceOf(Result::class, $resultB);
		self::assertInstanceOf(Result::class, $resultB2);
		self::assertInstanceOf(Result::class, $resultB3);

		self::assertTrue($resultA->isOk());
		self::assertFalse($resultA->isErr());

		self::assertFalse($resultB->isOk());
		self::assertTrue($resultB->isErr());

		self::assertFalse($resultB2->isOk());
		self::assertTrue($resultB2->isErr());

		self::assertFalse($resultB3->isOk());
		self::assertTrue($resultB3->isErr());

		self::assertEquals(123, $resultA->unwrap());
		self::assertInstanceOf(InvalidResultException::class, $resultB3->unwrapErr());
	}

	public function testOkOrElseThrowsUnwrapException()
	{
		$b = Option::none();
		$resultB = $b->okOrElse(function () { return new \RuntimeException('default err'); });

		self::expectException(UnwrapException::class);
		$resultB->unwrap();
	}

	public function testAnd()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->and(Option::some(456));
		$andB = $b->and(Option::some(456));

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isSome());
		self::assertTrue($andB->isNone());

		self::assertEquals(456, $andA->unwrap());
	}

	public function testAndThen()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->andThen(function ($v) { return Option::some($v * 2); });
		$andA2 = $a->andThen(function ($v) { return $v * 2; });
		$andB = $b->andThen(function ($v) { return Option::some($v * 2); });

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andA2);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isSome());
		self::assertTrue($andA2->isNone());
		self::assertTrue($andB->isNone());

		self::assertEquals(246, $andA->unwrap());
	}

	/*
	 * ->bind() is an alias for andThen
	 */
	public function testBind()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->bind(function ($v) { return Option::some($v * 2); });
		$andA2 = $a->bind(function ($v) { return $v * 2; });
		$andB = $b->bind(function ($v) { return Option::some($v * 2); });

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andA2);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isSome());
		self::assertTrue($andA2->isNone());
		self::assertTrue($andB->isNone());

		self::assertEquals(246, $andA->unwrap());
	}

	public function testAndThenTryWithValue()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->andThenTry(function () { return 456; });
		$andB = $b->andThenTry(function () { return 456; });

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isSome());
		self::assertTrue($andB->isNone());

		self::assertEquals(456, $andA->unwrap());
	}

	public function testAndThenTryWithNull()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->andThenTry(function () { return null; });
		$andB = $b->andThenTry(function () { return null; });

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isNone());
		self::assertTrue($andB->isNone());
	}

	public function testAndThenTryWithFalseAsFalse()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->andThenTry(function () { return false; });
		$andB = $b->andThenTry(function () { return false; });

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isSome());
		self::assertTrue($andB->isNone());

		self::assertEquals(false, $andA->unwrap());
	}

	public function testAndThenTryWithFalseAsNone()
	{
		$a = Option::some(123);
		$b = Option::none();

		$andA = $a->andThenTry(function () { return false; }, true);
		$andB = $b->andThenTry(function () { return false; }, true);

		self::assertInstanceOf(Option::class, $andA);
		self::assertInstanceOf(Option::class, $andB);

		self::assertTrue($andA->isNone());
		self::assertTrue($andB->isNone());
	}

	public function testOr()
	{
		$a = Option::some(123);
		$b = Option::none();

		$orA = $a->or(Option::some(456));
		$orB = $b->or(Option::some(456));

		self::assertInstanceOf(Option::class, $orA);
		self::assertInstanceOf(Option::class, $orB);

		self::assertTrue($orA->isSome());
		self::assertTrue($orB->isSome());

		self::assertEquals(123, $orA->unwrap());
		self::assertEquals(456, $orB->unwrap());
	}

	public function testOrElse()
	{
		$a = Option::some(123);
		$b = Option::none();

		$orA = $a->orElse(function () { return Option::some(456); });
		$orB = $b->orElse(function () { return Option::some(456); });
		$orB2 = $b->orElse(function () { return 456; });

		self::assertInstanceOf(Option::class, $orA);
		self::assertInstanceOf(Option::class, $orB);
		self::assertInstanceOf(Option::class, $orB2);

		self::assertTrue($orA->isSome());
		self::assertTrue($orB->isSome());
		self::assertFalse($orB2->isSome());

		self::assertEquals(123, $orA->unwrap());
		self::assertEquals(456, $orB->unwrap());
	}

	public function testIter()
	{
		$a = Option::some(123);
		$b = Option::none();

		$iterA = $a->iter();
		$iterB = $b->iter();

		self::assertInstanceOf(Iterator::class, $iterA);
		self::assertInstanceOf(Iterator::class, $iterB);

		self::assertEquals([123], $iterA->toArray());
		self::assertEquals([], $iterB->toArray());
	}

	public function testIterBy()
	{
		$a = Option::some(10);
		$b = Option::none();

		$iterA = $a->iterBy(
			function ($v) {
				for ($i = 0; $i < $v; $i++) {
					yield $i;
				}
			}
		);
		$iterB = $b->iterBy(
			function ($v) {
				for ($i = 0; $i < $v; $i++) {
					yield $i;
				}
			}
		);

		self::assertEquals([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], $iterA->toArray());
		self::assertEquals([], $iterB->toArray());
	}

	public function testApply()
	{
		$f1 = Option::some(function ($v) { return $v * 1.5; });
		$f2 = Option::none();
		$val = Option::some(8);
		$none = Option::none();

		$resF1Val = $f1->apply($val);
		$resF1None = $f1->apply($none);

		$resF2Val = $f2->apply($val);
		$resF2None = $f2->apply($none);

		self::assertEquals(12, $resF1Val->unwrap());
		self::assertTrue($resF1None->isNone());

		self::assertTrue($resF2Val->isNone());
		self::assertTrue($resF2None->isNone());
	}

	public function testApplicable()
	{
		$fn = function ($v) { return $v * 2; };
		$a = Option::from(123);
		$f = Option::from($fn);

		self::assertInstanceOf(
			Option::class,
			$f->apply($a)
		);

		self::assertEquals(
			246,
			$f->apply($a)
				->unwrap()
		);

		// Applicative map law
		self::assertEquals(
			$f->apply($a),
			$a->map($fn)
		);

		// Applicative identity law
		self::assertEquals(
			Option::from([F::class, 'identity'])
				->apply($a),
			F::identity($a)
		);

		// Applicative homomorphism law
		self::assertEquals(
			Option::from($fn)
				->apply($a),
			Option::from($fn(123))
		);

		// Applicative interchange law
		self::assertEquals(
			$f->apply($a),
			$f->from(function ($f) { return $f(123); })
				->apply($f)
		);

		// Applicative composition law
		// It needs a second function to test
		$f2 = Option::from(function ($x) { return $x + 5; });

		self::assertEquals(
			$f->from([F::class, 'compose'])
				->apply($f)
				->apply($f2)
				->apply($a),
			$f->apply($f2->apply($a))
		);
	}

	public function testMonadProperties()
	{
		$value = 10;
		$monad = Option::from(20);
		$fn1 = function ($v) { return Option::from($v + 10); };
		$fn2 = function ($v) { return Option::from($v * 2); };

		// Left identity
		self::assertEquals(
			$monad->from($value)
				->bind($fn1),
			$fn1($value)
		);

		// Right identity
		self::assertEquals($monad->bind([$monad, 'from']), $monad);

		// Associativity
		self::assertEquals(
			$monad->bind($fn1)
				->bind($fn2),
			$monad->bind(
				function ($x) use ($fn1, $fn2) { return $fn1($x)->bind($fn2); }
			)
		);
	}
}
 
