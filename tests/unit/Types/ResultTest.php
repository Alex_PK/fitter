<?php

namespace IteratorsTests\Unit\Types;

use Apk\Fitter\Exception\UnwrapException;
use Apk\Fitter\F;
use Apk\Fitter\Iterator;
use Apk\Fitter\Types\Result;
use Apk\Fitter\Types\Ok;
use Apk\Fitter\Types\Err;

class ResultTest extends \PHPUnit\Framework\TestCase
{
	public function testOk()
	{
		$a = Result::ok(123);
		self::assertTrue($a->isOk());
		self::assertFalse($a->isErr());
		self::assertEquals(123, $a->unwrap());
	}
	
	public function testErr()
	{
		$a = Result::err(new \RuntimeException('some error'));
		self::assertFalse($a->isOk());
		self::assertTrue($a->isErr());
	}
	
	public function testFrom()
	{
		$a = Result::from(123);
		self::assertTrue($a->isOk());
		self::assertFalse($a->isErr());
		self::assertEquals(123, $a->unwrap());
		
		$b = Result::from(new \RuntimeException('some error'));
		self::assertFalse($b->isOk());
		self::assertTrue($b->isErr());
	}
	
	public function testFromCalling()
	{
		$a = Result::fromCalling(function () { return 123; });
		$b = Result::fromCalling(function () { throw new \RuntimeException('some error'); });
		
		self::assertTrue($a->isOk());
		self::assertFalse($a->isErr());
		self::assertEquals(123, $a->unwrap());
		
		self::assertFalse($b->isOk());
		self::assertTrue($b->isErr());
	}
	
	public function testUnwrap()
	{
		$a = Result::from(123);
		$b = Result::from(new \RuntimeException('Error'));
		
		self::assertEquals(123, $a->unwrap());
		self::assertInstanceOf(\Exception::class, $b->unwrapErr());
	}
	
	public function testErrUnwrapThrowsUnwrapException()
	{
		$a = Result::err(new \RuntimeException('some error'));
		
		self::expectException(UnwrapException::class);
		$a->unwrap();
	}

	public function testUnwrapErrInAnOkResultThrowsException()
    {
        $a = Result::ok(123);

        self::expectException(UnwrapException::class);
        $a->unwrapErr();
    }

	public function testUnwrapOr()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		self::assertEquals(123, $a->unwrapOr(456));
		self::assertEquals(456, $b->unwrapOr(456));
	}
	
	public function testUnwrapOrElse()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		self::assertEquals(
			123,
			$a->unwrapOrElse(function () { return 456; })
		);
		self::assertEquals(
			456,
			$b->unwrapOrElse(function () { return 456; })
		);
	}
	
	public function testSome()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('Error'));
		
		$someA = $a->someOk();
		$errA = $a->someErr();
		
		$someB = $b->someOk();
		$errB = $b->someErr();
		
		self::assertTrue($someA->isSome());
		self::assertTrue($errA->isNone());
		
		self::assertTrue($someB->isNone());
		self::assertTrue($errB->isSome());
		
	}
	
	public function testMap()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		self::assertTrue(
			$a->map(function ($v) { return $v * 2; })
				->isOk()
		);
		self::assertFalse(
			$a->map(function ($v) { return $v * 2; })
				->isErr()
		);
		
		self::assertEquals(
			246,
			$a->map(function ($v) { return $v * 2; })
				->unwrap()
		);
		
		self::assertTrue(
			$b->map(function ($v) { return $v * 2; })
				->isErr()
		);
		self::assertFalse(
			$b->map(function ($v) { return $v * 2; })
				->isOk()
		);
	}
	
	public function testMapErr()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$mappedA = $a->mapErr(
			function ($e) { throw new \RuntimeException('new error', 0, $e); }
		);
		
		$mappedB = $b->mapErr(
			function ($e) { throw new \RuntimeException('new error', 0, $e); }
		);
		
		self::assertInstanceOf(Ok::class, $mappedA);
		self::assertEquals(123, $mappedA->unwrap());
		
		self::assertInstanceOf(Err::class, $mappedB);
	}
	
	public function testMapErrUnwrapThrowsUnwrapException()
	{
		$b = Result::err(new \RuntimeException('some error'));
		
		$mappedB = $b->mapErr(
			function ($e) { throw new \RuntimeException('new error', 0, $e); }
		);
		
		self::expectExceptionMessage('Unwrapping an error');
		$mappedB->unwrap();
	}
	
	public function testAnd()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$andA = $a->and(Result::ok(456));
		$andB = $b->and(Result::ok(456));
		
		self::assertInstanceOf(Result::class, $andA);
		self::assertInstanceOf(Result::class, $andB);
		
		self::assertTrue($andA->isOk());
		self::assertTrue($andB->isErr());
		
		self::assertEquals(456, $andA->unwrap());
	}
	
	public function testAndThen()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$andA = $a->andThen(function ($v) { return Result::ok($v * 2); });
        $andA2 = $a->andThen(function ($v) { return $v * 2; });
		$andB = $b->andThen(function ($v) { return Result::ok($v * 2); });
		
		self::assertInstanceOf(Result::class, $andA);
        self::assertInstanceOf(Result::class, $andA2);
		self::assertInstanceOf(Result::class, $andB);
		
		self::assertTrue($andA->isOk());
        self::assertTrue($andA2->isErr());
		self::assertTrue($andB->isErr());
		
		self::assertEquals(246, $andA->unwrap());
    }

    /*
     * ->bind() is an alias for ->andThen
     */
    public function testBind()
    {
        $a = Result::ok(123);
        $b = Result::err(new \RuntimeException('some error'));

        $andA = $a->bind(function ($v) { return Result::ok($v * 2); });
        $andA2 = $a->bind(function ($v) { return $v * 2; });
        $andB = $b->bind(function ($v) { return Result::ok($v * 2); });

        self::assertInstanceOf(Result::class, $andA);
        self::assertInstanceOf(Result::class, $andA2);
        self::assertInstanceOf(Result::class, $andB);

        self::assertTrue($andA->isOk());
        self::assertTrue($andA2->isErr());
        self::assertTrue($andB->isErr());

        self::assertEquals(246, $andA->unwrap());
    }

    public function testAndThenTry()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$andA = $a->andThenTry(function () { return 456; });
		$andB = $b->andThenTry(function () { return 456; });
		
		self::assertInstanceOf(Result::class, $andA);
		self::assertInstanceOf(Result::class, $andB);
		
		self::assertTrue($andA->isOk());
		self::assertTrue($andB->isErr());
		
		self::assertEquals(456, $andA->unwrap());
	}
	
	public function testAndThenTryThrows()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$andA = $a->andThenTry(function () { throw new \RuntimeException('Error'); });
		$andB = $b->andThenTry(function () { return 456; });
		
		self::assertInstanceOf(Result::class, $andA);
		self::assertInstanceOf(Result::class, $andB);
		
		self::assertTrue($andA->isErr());
		self::assertTrue($andB->isErr());
		
		self::expectException(UnwrapException::class);
		$andA->unwrap();
	}
	
	public function testOr()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$orA = $a->or(Result::ok(456));
		$orB = $b->or(Result::ok(456));
		
		self::assertInstanceOf(Result::class, $orA);
		self::assertInstanceOf(Result::class, $orB);
		
		self::assertTrue($orA->isOk());
		self::assertTrue($orB->isOk());
		
		self::assertEquals(123, $orA->unwrap());
		self::assertEquals(456, $orB->unwrap());
	}
	
	public function testOrElse()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$orA = $a->orElse(function () { return Result::ok(456); });
		$orB = $b->orElse(function () { return Result::ok(456); });
		
		self::assertInstanceOf(Result::class, $orA);
		self::assertInstanceOf(Result::class, $orB);
		
		self::assertTrue($orA->isOk());
		self::assertTrue($orB->isOk());
		
		self::assertEquals(123, $orA->unwrap());
		self::assertEquals(456, $orB->unwrap());
	}
	
	public function testOrElseTry()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$orA = $a->orElseTry(function () { return 456; });
		$orB = $b->orElseTry(function () { return 456; });
		
		self::assertInstanceOf(Result::class, $orA);
		self::assertInstanceOf(Result::class, $orB);
		
		self::assertTrue($orA->isOk());
		self::assertTrue($orB->isOk());
		
		self::assertEquals(123, $orA->unwrap());
		self::assertEquals(456, $orB->unwrap());
	}
	
	public function testOrElseTryThrows()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('some error'));
		
		$orA = $a->orElseTry(function () { throw new \RuntimeException('Error'); });
		$orB = $b->orElseTry(function () { throw new \RuntimeException('Error'); });
		
		self::assertInstanceOf(Result::class, $orA);
		self::assertInstanceOf(Result::class, $orB);
		
		self::assertTrue($orA->isOk());
		self::assertTrue($orB->isErr());
		
		self::assertEquals(123, $orA->unwrap());
	}
	
	public function testCatchOk()
	{
		$a = Result::from(123);
		$b = Result::from(123);
		$c = Result::from(123);
		$d = Result::from(123);
		
		$coughtA = $a->catch(Result::from(456));
		$coughtB = $b->catchThen(function () { return Result::from(456); });
		$coughtC = $c->catchThenTry(function () { return 456; });
		$coughtD = $d->catchThenTry(function () { throw new \InvalidArgumentException('Invalid'); });
		
		self::assertTrue($coughtA->isOk());
		self::assertTrue($coughtB->isOk());
		self::assertTrue($coughtC->isOk());
		self::assertTrue($coughtD->isOk());
		self::assertEquals(123, $coughtA->unwrap());
		self::assertEquals(123, $coughtB->unwrap());
		self::assertEquals(123, $coughtC->unwrap());
		self::assertEquals(123, $coughtD->unwrap());
	}
	
	public function testCatchErr()
	{
		$a = Result::from(new \RuntimeException('First error'));
		$b = Result::from(new \RuntimeException('Second error'));
		$c = Result::from(new \RuntimeException('Third error'));
		$d = Result::from(new \RuntimeException('Fourth error'));
		
		$coughtA = $a->catch(Result::from(123));
		$coughtB = $b->catchThen(function () { return Result::from(123); });
		$coughtC = $c->catchThenTry(function () { return 123; });
		$coughtD = $d->catchThenTry(function () { throw new \InvalidArgumentException('Invalid'); });
		
		self::assertTrue($coughtA->isOk());
		self::assertTrue($coughtB->isOk());
		self::assertTrue($coughtC->isOk());
		self::assertTrue($coughtD->isErr());
	}
	
	public function testIter()
	{
		$a = Result::ok(123);
		$b = Result::err(new \RuntimeException('Some error'));
		
		$iterA = $a->iter();
		$iterB = $b->iter();
		
		self::assertInstanceOf(Iterator::class, $iterA);
		self::assertInstanceOf(Iterator::class, $iterB);
		
		self::assertEquals([123], $iterA->toArray());
		self::assertEquals([], $iterB->toArray());
	}

	public function testIterBy()
	{
		$a = Result::ok(10);
		$b = Result::err(new \RuntimeException('Some error'));

		$iterA = $a->iterBy(
			function ($v) {
				for ($i = 0; $i < $v; $i++) {
					yield $i;
				}
			}
		);
		$iterB = $b->iterBy(
			function ($v) {
				for ($i = 0; $i < $v; $i++) {
					yield $i;
				}
			}
		);

		self::assertEquals([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], $iterA->toArray());
		self::assertEquals([], $iterB->toArray());
	}

    public function testApply()
    {
        $f1 = Result::ok(function ($v) { return $v * 1.5; });
        $f2 = Result::err(new \RuntimeException('Error function'));
        $val = Result::ok(8);
        $none = Result::err(new \RuntimeException('Error value'));

        $resF1Val = $f1->apply($val);
        $resF1None = $f1->apply($none);

        $resF2Val = $f2->apply($val);
        $resF2None = $f2->apply($none);

        self::assertEquals(12, $resF1Val->unwrap());
        self::assertTrue($resF1None->isErr());

        self::assertTrue($resF2Val->isErr());
        self::assertTrue($resF2None->isErr());
    }

    public function testApplicable()
    {
        $fn = function ($v) { return $v * 2; };
        $a = Result::from(123);
        $f = Result::from($fn);

        self::assertInstanceOf(
            Result::class,
            $f->apply($a)
        );

        self::assertEquals(
            246,
            $f->apply($a)
                ->unwrap()
        );

        // Applicative map law
        self::assertEquals(
            $f->apply($a),
            $a->map($fn)
        );

        // Applicative identity law
        self::assertEquals(
            Result::from([F::class, 'identity'])
                ->apply($a),
            F::identity($a)
        );

        // Applicative homomorphism law
        self::assertEquals(
            Result::from($fn)
                ->apply($a),
            Result::from($fn(123))
        );

        // Applicative interchange law
        self::assertEquals(
            $f->apply($a),
            $f->from(function ($f) { return $f(123); })
                ->apply($f)
        );

        // Applicative composition law
        // It needs a second function to test
        $f2 = Result::from(function($x) { return $x + 5; });

        self::assertEquals(
            $f->from([F::class, 'compose'])->apply($f)->apply($f2)->apply($a),
            $f->apply($f2->apply($a))
        );
    }

	public function testMonadProperties()
	{
		$value = 10;
		$monad = Result::from(20);
		$fn1 = function ($v) { return Result::from($v + 10); };
		$fn2 = function ($v) { return Result::from($v * 2); };

		// Left identity
		self::assertEquals(
			$monad->from($value)
				->bind($fn1),
			$fn1($value)
		);

		// Right identity
		self::assertEquals($monad->bind([$monad, 'from']), $monad);

		// Associativity
		self::assertEquals(
			$monad->bind($fn1)
				->bind($fn2),
			$monad->bind(
				function ($x) use ($fn1, $fn2) { return $fn1($x)->bind($fn2); }
			)
		);
	}
}